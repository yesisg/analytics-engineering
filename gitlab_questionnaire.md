### Gitlab Questionnaire
## Yesenia Garcia

**1.** **What about the GitLab Data & Analytics team particularly interests you?**

Collaboration and transparency. Working on a product that I use every day and going to the source of best practices is what interests me most. I’ve had the opportunity to work with Gitlab platform to implement standardized ETL automation for an analytics team and every commit, every production release incites myself and others to contribute more. Gitlab’s remote company culture stories also impressed me.

**2.** **What analytic reports (SaaS-related or otherwise) have you previously delivered for a company, who was the recipient, and what are some challenges you faced?**

My main role at Nordstrom is to deliver a suite of clickstream (Snowplow) reports using Tableau to Digital Merchandisers, Product Managers, Engineers, and internally to BI. One of the main challenges has been monitoring tag implementation quality and data size restraints. Historically, tag management has relied on self-service implementation which requires close, and early relationships with product and engineering teams. Data size restraints are handled on a case-by-case basis by BI. I’ve designed a roadmap for BI to provide a suite of reports and access to self-service tools for high-level consumers and deep-dive sources for analysts looking for lower granularity, solving most challenges.

**3.** **Explain the typical data analysis process along with some common problems that data analysts encounter during analysis.**

A typical process involves requirements gathering, data discovery, model, testing, peer/business review, output delivery. The most common struggle I have seen lies in the analysts ability to interpret the end state. Many times problems arise because stakeholders and/or analyst inflate scope with complex models and outputs, missing out on valuable data insights. Instead, the goal should be in delivering for quality and maintainability, and incremental iteration.

**4.** **Discuss a time you had someone question your analytic work and the process you took to explain your process.**

Many times during crucial events and soft business periods questions arise about our work. I approach each inquiry with a multi-step assessment: What is the business concern? What is the business expectation? What are steps to resolution/understanding the problem?

Recently, during Nordstrom's biggest event of the year, I was asked to review discrepancies between two different demand (web sales) figures reported for last year (2018), inaccurately summarizing year-over-year performance. It is company accepted to have 2-3% variance between data sources, but in this case the variance was 12%. My first step was to review both models for differences in logic. What I found was nearly 5% of new (to last year's event) product SKUs with web initiated orders were not captured by clickstream logs, during the first and biggest weekend of our event. However, those same product SKUs were available in events table. To capture missing product orders, reconcile differences and avoid another coverage miss, I integrated events as source of truth for demand alongside existing demand, creating two metrics: `web_demand` and `event_demand`. Next, a 4-year backfill was done to ensure all dependent models were up-to-date with new source of truth. Now the business is able to track both demand numbers to understand gaps in data coverage while maintaining alignment with other demand sources.

I believe each question should be answered with full transparency. History and documentation are reviewed, and steps to reproduction/resolution are shared with all teams involved.

**5.** **What is the most difficult data analysis problem that you have solved to date and how did you do it?**

Porting navigational path model to support clickstream, database and site tree manager platform migrations, complicated further by data integrity concerns.

One of the first challenges was reconciling expected differences between third-party and internally developed site tree manager platforms - output values. Vendor values were tilde delimited numerical record and internally we passed a friendly breadcrumb name record, as the customer would see on the website. Different navigation path values created a challenge to compare year-over-year performance, especially during our biggest event, Anniversary. The solve was in finding a unique, persistent dimension available in both platforms and clickstream databases pre/post migration. I chose `urlpath` as my persistent dimension. I created an as-is 1:1 mapping of every navigation path available, regardless of the platform, giving us the ability to use all historical data.

Avoiding stitching together a lookup for different navigation values saved analyst resources and also allowed me to present engineering teams with active coverage maps as migration progressed. What navigation paths are seeing most traffic? What navigations paths are dropping off during implementation phases? Etc.

**6.** **What books, blogs, or sites have you read in the past year to improve your data knowledge?**

* [Automate the boring stuff with Python](https://automatetheboringstuff.com/)
* [Data Science from Scratch](https://www.oreilly.com/library/view/data-science-from/9781492041122/)
* [A Beginners Guide to Data Engineering](https://medium.com/@rchang/a-beginners-guide-to-data-engineering-part-i-4227c5c457d7)
* [Tableau Viz of the Day](https://public.tableau.com/en-us/gallery)
* [FiveThirtyEight](https://fivethirtyeight.com/) - [Data](https://data.fivethirtyeight.com/)
* [Flowing Data](https://flowingdata.com)
* [FanGraphs](https://www.fangraphs.com/)

**7.** **Please check our main project  [https://gitlab.com/meltano/analytics/](https://gitlab.com/meltano/analytics/) and find something that, were you to be hired, you’d be interested to work on and contribute to. Why did you choose that?**

 [snowflake-dbt/macros](https://gitlab.com/gitlab-data/analytics/tree/master/transform/snowflake-dbt/macros)  - After watching live demo detailing dbt integration with gitlab schema, I was super excited to learn more. I have spent much of my time as an analyst leading the charge in creating reusable resources and investing time in documentation. Too many data teams work in silos and rely on tribal knowledge to understand their data which leads to disparate sources of truth and duplication of effort. Macros is one of the first things I would love to work on and contribute to.

**8.** **Please spend no more than 5 minutes preparing one SQL query in Snowflake SQL syntax for this question. Given a table called sfdc_oppotunity_xf in the analytics schema with the   , which month in the last year had the highest average IACV for closed won deals?** [information as documented](https://gitlab-data.gitlab.io/analytics/dbt/snowflake/#!/model/model.gitlab_snowflake.sfdc_opportunity_xf)

```sql
WITH sfdc_opportunity AS (

	SELECT *
	FROM "ANALYTICS".analytics.sfdc_opportunity_xf

), month_avg_iacv

    SELECT
        , DATE_PART(month, close_date) AS close_month
        , AVG(incremental_acv) AS avg_iacv
    FROM sfdc_opportunity
    WHERE is_won = TRUE
        AND close_date >= DATE_TRUNC('year',CURRENT_DATE)
    GROUP BY 1
    ORDER BY 2 DESC
)

SELECT *
FROM month_avg_iacv
LIMIT 1
```
