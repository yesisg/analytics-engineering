#LISTS, TUPLES, AND SETS
courses = ['History', 'Math', 'Physics', 'CompSci']

# slicing
print(courses[0:2])  # ['History', 'Math']
print(courses[:2])  # ['History', 'Math']
print(courses[2:])  # ['Physics', 'CompSci']

# append to end of list
courses.append('Art')

#insert in position
courses.insert(0, 'Art')
courses_2 = ['Art', 'History']

# inster list into lists
courses.insert(0, courses_2)

# extend values to list
courses.extend(courses_2)

# pop - removes value at end 1 at a time
popped = courses.pop()

# sort
nums = [1, 5, 2, 4, 3]

courses.sort()
nums.sort()

print(courses)
print(nums)

# Index value position
print(courses.index('CompSci'))

# True/False
print('Art' in courses)
print('Math' in courses)

# For Loop. access each value
for item in courses:
    print(item)

# Enumerate - returns index, value
# start with 1, not 0
for index, course in enumerate(courses, start=1):
    print(index, course)

# Create comma separated string
course_str = ', '.join(courses)

# Split on comma space
new_list = course_str.split(', ')

print(course_str)
print(new_list)
