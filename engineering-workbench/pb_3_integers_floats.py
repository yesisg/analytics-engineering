# integer = whole number
# float = decimal

num = 3.14
print(type(num))

# arithemetic
# modulo % = gives remainder of division. use case is to tell if number is even or odd. Divide by 2. if 0 then even elseif 1 then odd
print(3 % 2)
print(3 / 2)

# order of operation apply
print(3 * 2 + 1) # 7

print(3 * (2 + 1)) # 9

# incrementing by 1

num = 1
num += 1 # shorthand for num + 1
print(num)

# absolute values
# rounds up. apply to what digit to round
print(round(3.75)) # 4
print(round(3.75, 1)) # 3.8

# compare
num_1 = 3
num_2 = 2

print(num_1 == num_2) # double = to compare
print(num_1 != num_2)
print(num_1 > num_2)
print(num_1 < num_2)

# strings
num_1 = '100'
num_2 = '200'

print(num_1 + num_2) # adding strings is same as concat

# cast strings to add 
num_1 = int(num_1)
num_2 = int(num_2)

print(num_1 + num_2) # adding cast int
