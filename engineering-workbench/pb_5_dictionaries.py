student = {'name': 'John', 'age': 25, 'courses': ['Math', 'CompSci']}

print(student)

# return value using key
print(student['name'])

# key that doesn't exist with error
print(student['phone'])

# get to return custom error
print(student.get('phone', 'Not found'))

# create new key value
student['phone'] = '555-5555'
student['name'] = 'Jane' # if exists, it will update value

# update shorhand
student.update({'name': 'Jane', 'phone': '555-5555'})

# remove key value
del student['age']

age = student.pop('age')
print(age) # see removed

# len how many keys
print(len(student))

# keys
print(student.keys())

#values
print(student.values())

# key values
print(student.items())


# loop key and values
for key, value in student.items():
  print(key, value)