""" EMPTY LISTS[], TUPLES(), SETS{}

"""

# Empty Lists
empty_list = []
empty_list = list()

# Empty Tuples
empty_tuple = ()
empty_tuple = tuple()

# Empty Sets
empty_set = {} # WRONG! This is a dict
empty_set = set() # RIGHT use built in set()