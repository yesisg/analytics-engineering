""" SETS {}
Unordered and no duplicates. 
Used to test if a value is part of set; More efficient than lists, tuples.
"""
cs_courses = {'History', 'Math', 'Physics', 'CompSci'}
art_courses = {'History', 'Math', 'Art', 'Design'}

# Used to test if a value is part of set; More efficient than lists, tuples.
print('Math' in cs_courses)

# Same in both SETS
print(cs_courses.intersection(art_courses))

# Differences in SETS
print(cs_courses.difference(art_courses))

# Combine SETS
print(cs_courses.union(art_courses))

