nums = [1, 2, 3, 4, 5]

# loop
# break - stops iteration
# continue - continues itiration
for num in nums:
  if  num == 3: 
    print('Found!')
    continue # break, continue
  print(num)


# loop within a loop
for num in nums:
    for letter in 'abc':
        print(num, letter)

# range
for i in range(10):
  print(i)

for i in range(1, 11):
  print(i)

# while loops
# continues until false or break
# interrupt infinite loop - CTRL C
x = 0

while x < 10:
  if x == 5:
    break
  print(x)
  x += 1

