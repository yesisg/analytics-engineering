```mermaid
graph TB
subgraph "Product Level"
c1(( )) --- c2[Product Views]
c2 --> c3[Cart Adds]
c3 --> c4[Orders]
end
subgraph "Session Level"
s1((Sessions)) -->s2[Viewing Sessions]
s2 --> s3[Add Sessions] 
s3 --> s4[Buying Sessions]
end

subgraph "Visitor Level"
t1((Visitors)) -->t2[Viewing Visitors]
t2 --> t3[Add Visitors]
t3 --> t4[Buying Visitors]
end

subgraph Funnel
f1((Traffic)) --> f2[Product or Page Views]
f2 --> f3[Cart Adds]
f3 --> f4[Orders]
end
```