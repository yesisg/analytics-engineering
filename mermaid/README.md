## Mermaid

Mermaid is a simple markdown-like script language for generating charts from text via javascript.
[Learn more](https://mermaidjs.github.io/#/README)