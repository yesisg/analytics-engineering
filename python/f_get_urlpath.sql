-- python udf for getting query parameters
SET SEARCH_PATH TO bi_prd;
CREATE OR REPLACE FUNCTION f_get_urlpath (page_url varchar(MAX))
    returns varchar(5000)
IMMUTABLE
AS $$
    if page_url is None:
        return None
    else:
        from urlparse import urlparse, parse_qs
        try:
            parsed = urlparse(page_url)
            if len(parsed.path):
                return parsed.path
        except:
            return None
$$ language plpythonu
;

GRANT EXECUTE ON FUNCTION f_get_urlpath (page_url varchar(MAX)) TO PUBLIC
;
