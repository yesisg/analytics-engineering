---
layout: wiki
title: Attribution
toc: true
category: methodology
tags: kpi funnel attribution metrics directional sequential actual
---

## Introduction
Details of common KPI attribution logic used by BI across swimlanes.

## Attribution methods

|**Attribution**| **Definition** | **Report Example** |
|---------|----------------|-----------|
| Actual | Total aggregate without regard for distribution | Topline, Daily Funnel |
| Directional | Equal attribution to all activity between analysis segment first timestamp and order timestamp | Content, Digital Page Interaction |
| Multi-touch | _coming soon_ | |
| Sequential | Capture beginning of Product Page (Funnel): `Product View` to `Add to Cart` to `Order` in timestamp sequence | WBR, Category/Breadcrumb |

Learn more about [Aggregation Levels and Funnels](/data-docs/w/methodology/level-of-aggregation.html)

## Actual
Actual attribution is total aggregate, as seen in Topline or Daily funnel.The most common segment is platform.

{% asset methodology-attribution-actual.png %}

## Directional
Directional method attributes any activity between analysis segment timestamp and order timestamp. Attribution timestamp window resets after order is placed to avoid attributing to post-order activity.

For example: customer lands on Homepage, browses 5 pages, views 10 product pages, places and order for a total of $135. All activity gets $135 Demand attribution. Generally, Product Detail Pages (PDP) are not a segment for directional attribution.

```mermaid
graph LR
d1[Directional] .->| all receive attribution| d2[DLP]
d2 .- dc2($135)
d2 --> d3[Category]
d3 .- dc3($135)
d3 --> d4(PDP)
d4 --> d5[Category]
d5 .- dc5($135)
d5 --> d6[Search]
d6 .- dc6($135)
d6 --> d7(PDP)
d7 -->d8((Cart Adds))
d8 --> d9((Order Total: $135))
```

```sql
--SQL example Directional attribution
SELECT page_views.derived_date, COUNT(DISTINCT orders.order_id) orders

FROM sp_page_views page_views

LEFT JOIN sp_orders orders
	ON page_views.derived_date = orders.derived_date
	AND page_views.app_id = orders.app_id
	AND page_views.domain_userid = orders.domain_userid
	-- join orders that occured after page view
	AND page_view.derived_tstamp < order.derived_tstamp
```


## Sequential
Perhaps the most commonly used, Sequential Attribution starts at product page and follows the customer journey from there. This is closest aligned to [Funnel at Product Level](/data-docs/w/methodology/level-of-aggregation.html#funnels).

```mermaid
graph LR
c1(( )) .- c2[Product Views]
c2 --> c3[Cart Adds]
c3 --> c4[Orders]
```

<br>
This method allows us to track customer behavior starting at Product Level and follow the funnel or analyse the activity immediately preceding. Using [Breadcrumb reporting](/data-docs/w/reports/product-breadcrumb.html) and Search reporting as an example, attribution goes to segment immediatelly preceeding Product View.

```mermaid
graph LR
d1[Sequential] .->| Segment immediately <br>preceeding Product View <br>gets attribution| d2[DLP]
d2 --> d3[Category]
d3 .- dc3($135)
d3 .- dpv(prod views: 40K)
d3 --> d4(Product Page)
d4 -->dc4((Cart Adds))
dc4 --> d5[Category]
d5 --> d6[Search]
d6 .- dpv6(prod views: 70K)
d6 --> d7(Product Page)
d7 --> d8((Order Total: $135))
```


Code snippet is multi-step and best viewed at the source: [Breadcrumb Funnel]()

