---
layout: wiki
title: Level of Aggregation
toc: true
category: methodology
tags: kpi funnel topline summary demand udv visitors conversion product views orders units cart bag sessions page pageview measures metrics
---

## Introduction
There are three levels of aggregation used in BI reporting: Visitor, Session, and Count. This article defines each measure along with it's relevancy at different points in the funnel.

## Reporting Levels
Tools are aggregated at different levels dependent on the objective. For example, Weekly Scorecard is designed at a Visitor level while Category Performance is designed at a Count level. 

|**Level**| **Definition** | **Aggregate** |
|---------|----------------|-----------| 
| Visitor | Individuals browsing the site. Derived by counting distinctdomain_userid values (cookies)	This is used over UDV so that the same term can be used across analyses where the aggregation level is not daily. | `COUNT(DISTINCT domain_userid)`
| Session | One continuous visit by one Visitor (aka cookie) <br/> Visitor can contain multiple sessions; Session ends after 30 mins inactivity OR at midnight | `COUNT(DISTINCT domain_sessionid)`|
| Count | Aggregate of metric without regard for duplicates | `COUNT(*)` |

## Funnels
Funnel is used to follow customers from traffic to the site to placing an order. In Visitor and Session funnels, measure begins at site entry. In Product, measure begins only once customer views a product page. _Product Funnel is synonomous with Count Level in [Levels of Aggregation](#level-of-aggregation) table in next section._

```mermaid
graph TB

subgraph "Product Level"
c1(( )) --- c2[Product Views]
c2 --> c3[Cart Adds]
c3 --> c4[Orders]
end

subgraph "Session Level"
s1((Sessions)) -->s2[Viewing Sessions]
s2 --> s3[Add Sessions] 
s3 --> s4[Buying Sessions]
end

subgraph "Visitor Level"
t1((Visitors)) -->t2[Viewing Visitors]
t2 --> t3[Add Visitors]
t3 --> t4[Buying Visitors]
end

subgraph Funnel
f1((Traffic)) --> f2[Product or Page Views]
f2 --> f3[Cart Adds]
f3 --> f4[Orders]
end
```


## Levels of Aggregation
 In this table, most used metrics are broken out to display how the measurement changes based on the level of aggregation desired. 

|**Measure**|**Definition**|**Visitor Level**|**Session Level**|**Count Level**|
|---------|----------------|-----------| -----| ----- |
| Traffic | Incoming Traffic to the Site| Visitors | Sessions | Page Views |
| Product Page Views | Traffic to Product Page | Viewing Visitors | Viewing Sessions | Product Views |
| View Rate | % of traffic that viewed at least one Product Page | `Viewing Visitors/Visitors` | `Viewing Sessions/Sessions` | `Product Views/Page Views`|
| Instock Rate | % of Prod Views for which desired SKU was in stock* | `Instock Views/Product Views` | `Instock Views / Product Views` | `Instock Views / Product Views` |
| Add to Cart| Items added to cart | Adding Visitors | Adding Sessions | Cart Adds|
| Cart Add Rate | % of Viewing Visitors that Add at least one item to cart |`Add Visitors/Viewing Visitors` | `Add Sessions/Viewing Sessions`| `Cart Adds/Product Views`|
| Orders | Distinct Orders placed | Buying Visitors| Buying Sessions| Units | 
| Completion Rate |  % of Adding Visitors that place an Order | `Orders/Add Visitors`|`Orders/Add Sessions`|`Units/Cart Adds`|
|Conversion Rate| % of Traffic that place an Order| `Orders/Visitors`|`Orders/Sessions`|`Units/Product or Page Views`|
| Demand | Total line item demand across all web orders |Demand|Demand|Demand|
| AOV | Average Order Value | `Demand/Orders` | `Demand/Buying Sessions`| |
|Units | Total quantity of all line items, representing individual units sold. (aka Items Sold) | Units | Units | Units |
|AOU | Average Order Units | `Units/Orders` | `Units/Buying Sessions` | |
|ASP| Average Selling Price of items sold | `Demand/Units` | `Demand/Units`| `Demand/Units`|
|Avg Visit Value | Average Value of Traffic to site| `Demand/Visitors` | `Demand/Sessions`|`Demand/Product or Page Views`|
