## Introduction

Many metrics are reported across different areas of the business. Use the tables below to find metric definitions.

## Common KPIs

### Topline Metrics

|**Metric**|**Description**|**Area**|
|--|--|--|
|Visitors | Individuals browsing the site. Derived by counting distinct domain_userid values (cookies). Note: This is used over UDV so that the same term can be used across analyses where the aggregation level is not daily.|All|
|Sessions | Sessions of users browsing the site. Derived by counting distinct domain_sessionid values.| All |
|Page Views | Count of page views. | All |
|Product Views | Count of page views on the product page. | All|
|Add to Cart |  Items added to cart | All |
|Demand | Total line item demand across all web orders. | All |
|Units | Total quantity of all line items, representing individual units sold. (aka Items Sold) | All |
|Orders | Distinct orders. | All |
|Link Clicks | Total clicks of a link/element. | All |
|Updates | App updates. | Mobile App |
|Downloads | App downloads| Mobile App |

### Frequently Used Metrics

|**Metric**|**Description**|**Area**|**Aggregation**|
|--|--|--|--|
|Add to Cart Rate |  Rate at which customers is adding units to the cart compared to how much products are viewed. | All | Ratio |
|Average Order Value | AOV - Demand/Orders | All | Average |
|Average Order Units | AOU - Orders/Units | All |Average |
|Average Selling Price | ASP - Demand/Units (or Items Sold) | All | Average|
|Bounce Rate | Percentage of total visits that were single page visits ( Single Page Visits / Visitors ) | All |Ratio|
|Buying Visitors  |  Visitors who placed an order  |  All | Count |
|Cart Adds | Items added to cart or bag | All | Count |
|Adding Visitors | Visitors Count of visitors that added items to cart or bag (aka Unique Adders) | All | Count |
|Checkout Abandonment Rate | Rate at which visitors exited checkout without completing purchase | All | Ratio |
|Completion Rate | Rate at which customers order compared to add-to-bag actions |All | Ratio |
|Conversion Rate | Rate of which desired outcome was accomplished <br> Traffic = Orders/Visitors (aligns with Finance) <br/> <br> Product = Items Sold/Product Views <br/> |All | Ratio |
|Downloads | App downloads |  Mobile App | Count |
|Entry Page | Count of visits which page is first page of their cookies | All | Count|
|Entry Rate | Rate at which page is FIRST page of a customers visit to the site |All | Ratio|
|Instock Rate |Percentage of product views that were instock when they were viewed (InStock Views / Product Views) | All | Ratio |
|Instock Views | Product View during a day where we had at least one of the following true for item (either at the SKU or SG level): Had at least one unit in stock at one or more FLS stores OR Was on dropship OR Had at least one unit in stock at Cedar/ECFC <br>We don’t know how much the vendor had in stock but we know whether or not it had a positive dropship feed on a given day. <br/>| All | Ratio|
|Product View Rate  | Percentage of Traffic that views at least one product page |  All | Ratio |
|Screen Views | App views | Mobile App | Count |
|Single Page Visits | Visitors that only visited page or site once before exiting aka Bounces|All| Count|
|Units | Total quantity of all line items, representing individual units sold. (aka Items Sold)  |All | Count |
|Updates | App updates | Mobile App | Count |
|View Rate  |Rate at which desired page was viewed as a percent of all views |All |Ratio|
|Viewing Sessions | Sessions that viewed a product page | All | Count |
|Viewing Visitors | Visitors who viewed Product page | All | Count|

## Segmentations

Many segmentations are calculated on a regular basis, and are re-used in downstream analyses. These are generally availabile at the lowest possible level of granularity for which they can be calculated.

|Metric|Granularity|Table|Description|
|-----------|------|-----|-----------|
|**Visit Type**|Cookie/Day|`bi_prd.sp_visit_type` (Snowplow)<br>`bi_prd.visit_type` (CoreMetrics)|365-day lookback to see whether a particular cookie is a new or repeat visitor.|
|**Custkey Platforms**|Custkey/Day|`bi_prd.custkey_platforms`|Shows which platforms the customer key has shown up on in the past 90/30/7 days.|
|**Top 20 Percent**|Custkey/Day|`bi_prd.top_20_percent_custkeys`|Tracks the top 20% of customers by total digital spend across all platforms. Does not include in-store purchases.|