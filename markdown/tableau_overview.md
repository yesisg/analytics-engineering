---
layout: wiki
title: Tableau
toc: true
category: tools
tags: tableau desktop server visualization viz dashboard tool uxo
---

## Introduction

Tableau is a Business Intelligence & Data Visualization application used internally for creating reports and dashboards, as well as providing self-service data capabilities.

You can see published materials on Tableau Server here:

* [NordACE Digital](https://tableau.com)
* [NordACE Marketing](https://tableau.com)

![Tableau Screenshot](https://cdns.tblsft.com/sites/default/files/pages/maps2small.png)

## Access

There are multiple different types of access for Tableau.

* [Tableau Server](https://tableau.com) is available to everyone internally who has a `@company.com` email.
* Tableau Desktop is requisitioned on a per-user basis. You can have your manager put in a request for your license at the [ServiceNow Software Page]().


## Resource Guide

### Connecting to Data Source
It is possible to connect to Redshift, Teradata, `.csv` or Excel files, to name a few. Tableau Admins do not administer or maintain data source credentials. Learn more about [Supported Connectors](https://onlinehelp.tableau.com/current/pro/desktop/en-us/exampleconnections_overview.htm)

#### Connect to Redshift, Teradata, MADM, CA 
Use the same connection string and credentials to log in via Tableau. See sidebar menu for data source info.

### Blending vs Joining
The following guide assumes that your data has been summarized to the desired granularity. 

Assess your data and consider the following:
- Where your data is coming from
- How many connections you have in your workbook
- The number of records you have in the data

|**Situation (suggestion to try first)**|**JOIN**|**BLEND**|
|-------------|--------|---------|
| More than one table from the same data source|:thumbsup: |  |
| Adding data to an existing data source |:thumbsup: | |
| Multiple data types for one worksheet| |:thumbsup: |
| Showing summary and details together | |:thumbsup:|

Common situations that may perform better with data blending include the following:
- The data source contains too many records for a join to be practical.
- You want to display a summary and details at the same time.

>Note: If one of the data sources that you want to use for data blending is a cube, the cube must be the primary data source.

Learn more about:
- [Deciding Between Joining Tables and Blending Data](https://kb.tableau.com/articles/howto/deciding-between-joining-tables-and-blending-data)
- [Blending on Summary Data](https://onlinehelp.tableau.com/current/pro/desktop/en-us/multipleconnections_summarydata.htm)
- [Joining in Tableau](https://onlinehelp.tableau.com/current/pro/desktop/en-us/joining_tables.htm)

### Level of Detail (LOD) Expressions
Level of Detail (LoD) calculations are a powerful extension of Tableau's functionality, and many of the ways that they can be useful are as a result of their placement in Tableau's Order of Operations.  A good introduction to LoD calculations is available as a tutorial on Tableau's website: [Top 15 LoD Expressions](https://www.tableau.com/about/blog/LOD-expressions)

Importantly, the `FIXED` and `INCLUDE/EXCLUDE` flavors of LoD actually occur in different places in this hierarchy, and as a result can interact in meaningful ways.  The basic order of operations for Tableau's filters and LODs is visualized below:

**LOD Order of Operations**

```mermaid
graph LR
t1[Data Source] --- t2[Data Source Filters]
t2 --- t3[Context Filters ]
t3 ---t4
t4{Fixed}
t4 --- t6[Dimensions Filters]
t8{Include}
t9{Exclude}
t6 ---t8
t6 --- t9
t8 --- t10[Measure Filters]
t9 --- t10[Measure Filters]

style t1 fill:#c2d6d6, stroke:#669999, stroke-width:3;
style t2 fill:#c2d6d6, stroke:#FFF;
style t3 fill:#c2d6d6, stroke:#FFF;
style t6 fill:#c2d6d6, stroke:#FFF;
style t10 fill:#c2d6d6, stroke:#FFF;
```

Crucially, Dimension Filters are executed between `FIXED` and `INCLUDE/EXCLUDE` calculations.  Subsequent pages in this space will offer some examples that leverage this difference, but here are some things to keep in mind:

 - `FIXED` calculations happen before Dimension Filters; as a result, unless you promote the pills in your filter shelf to “Context Filters”, they will be ignored.

 - `INCLUDE` and `EXCLUDE` calculations happen after Dimension Filters like any other measure in Tableau (except `FIXED`).  If you want filters to apply to your Level of Detail expression but don’t want to use Context Filters, you can rewrite your calculation using `EXCLUDE` or `INCLUDE` keywords.