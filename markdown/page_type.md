---
layout: wiki
title: Page Type
toc: true
category: methodology
tags: page type categories content segmentation
---

## Introduction

Nordstrom.com pages are identified by their content. For example, Content page contains editorial content, images really, and no product; Category contains products and filters, Product pages contain the actual product, so on and so on.

## Deriving Page Type

To identify these pages we look at the `page_urlpath`. Secure is the only page type where `urlhost` is used over `page_urlpath`. Use this logic to define page type by their classification between first two slashes
e.g. /c/womens-dresses-shop contains `c`, which means it's a category page.

```sql
CASE
    WHEN page_urlhost LIKE '%secure.nordstrom.com' THEN 'SECURE'
    WHEN page_urlpath = '/' THEN 'HOMEPAGE'
    WHEN page_urlpath LIKE '%~%' THEN 'FILTERED BRANDS'
    WHEN REGEXP_COUNT(page_urlpath, '/') = 3 AND LEFT(page_urlpath, 3) = '/c/' THEN 'FILTERED CATEGORY'
    ELSE
      (
        CASE lower(split_part(page_urlpath, '/', 2))
            WHEN 'c'                            THEN 'CATEGORY'
            WHEN 'content'                      THEN 'CONTENT'
            WHEN 's'                            THEN 'PRODUCT'
            WHEN 'sr'                           THEN 'SEARCH'
            WHEN 'ssr'                          THEN 'SEARCH'
            WHEN 'th'                           THEN 'THEME RESULTS'
            WHEN 'stores'                       THEN 'STORES'
            WHEN 'store'                        THEN 'STORES'
            WHEN 'st'                           THEN 'STORE DETAILS'
            WHEN 'store-details'                THEN 'STORE DETAILS'
            WHEN 'o'                            THEN 'OUTFIT'
            WHEN 'os'                           THEN 'CHECKOUT'
            WHEN 'm'                            THEN 'MODAL'
            WHEN 'app'                          THEN 'CHAT'
            WHEN 'careers'                      THEN 'CAREERS'
            WHEN 'rewards'                      THEN 'REWARDS'
            WHEN 'choose-three-free-samples'    THEN 'GIFT WITH PURCHASE'
            WHEN 'styleboard'                   THEN 'STYLE BOARDS'
            WHEN 'gift-card'                    THEN 'GIFT CARDS'
            WHEN 'nordstrom-gift-cards'         THEN 'GIFT CARDS'
            WHEN 'yourlook'                     THEN 'LOOKS'
            WHEN 'look'                         THEN 'LOOKS'
            WHEN 'aboutus'                      THEN 'ABOUT'
            WHEN 'services'                     THEN 'SERVICES'
        ELSE 'OTHER'
        END
      )
END
```
> Note: `c` is used in many instances other than Category.

#### Secure
Any of the area of the site that is accessed by user only once they have successfully logged into their account.
Secure is defined by `page_urlhost` = `secure.nordstrom.com`.

#### Homepage
Homepage is, well, homepage. We use `page_urlpath = '/'` to identify homepage over Page Title, mostly because of case consistency issues during migration to Snowplow.

#### Filtered Brands
Result of pre-filtered categories pages set to to display one brand's items. These pages are often created for SEO purposes but can also be launched by righ-clicking on a brand filter and using 'open-in-new-tab'.
This action does not fire tags and the URL will look something like: https://shop.nordstrom.com/c/womens-shoes/adidas~1137

#### Filtered Category
Similar to Filtered Brands above, Filtered Categories are the result of pre-filtered categories pages set to to display a set of results basefd on a filter criteria, such as color, type, price, etc. These pages are often created for SEO purposes but can also be launched by righ-clicking on a filter and using 'open-in-new-tab'.
This action does not fire tags and the URL will look something like: https://shop.nordstrom.com/c/womens-shoes/casual


#### Category

`c`

These pages contain product and filter engagements.
*Sample URL: https://shop.nordstrom.com/c/mens-shoes*

{% asset methodology-page-type-category.png %}


#### Content

`content` `c`

These pages are full of editorial content (images) and ***do not*** directly link to product page or contain filter engagements.
*Sample URL: https://shop.nordstrom.com**/content/**whats-now-editorial*

> Note: all content pages have reverted back to using `c`. Between 6/11/2018 and 8/10/2018 `content` was used.

{% asset methodology-page-type-content.png %}


#### Product

`s`

Pages with Product. There are many ways a customer can land on product page, such as Category, Recs (global footer), Looks, etc. Customer are not able to land on Product from Content pages, unless it's from shoppable content.

{% asset methodology-page-type-product.png %}

#### Search

`sr` or `ssr`

Results page from Search query. Note: If a customer searches for a brand, redirects occur and the customer will be pointed to a brand Category Page and not Search results.

{% asset methodology-page-type-search.png %}

#### Modal

`m` `c`

Modals are pop-ups that keeps the user on page. These register Link Clicks, but tags associated with the opening of a Modal do not fire - only when a customer right-click open in new tab will a tag fire. Learn more about [Modals](https://nordace.prod.dots.vip.nordstrom.com/data-docs/w/reports/modals.html)

> Note: Modals are no longer solely identified by `m`. Change occured around start of Anniversary 2018.

{% asset methodology-page-type-modal.png %}

#### Looks

`look` or `yourlook`

Looks are the result of a loaded (widget) below product in [Product](#product) page, on [Content](#content) pages, or Your Look profile in [Secure](#secure).

{% asset methodology-page-type-looks.png %}

#### Others
* **Store** `st` `store-details` or `store` `stores`- Store locator
* **Chat** `app` - Pop-up support window
* **Careers** `careers` - Careers page
* **Rewards** `rewards` - Nordstrom rewards and loyalty program
* **Gifts With Purchase** `choose-three-free-samples` - Selection of free gift
* **Style Boards** `styleboard`
* **Gift Cards** `gift-card` or `nordstrom-gift-card` - Gift card pages contain only element tags. Click data may be available in `sp_link_clicks`
* **About** `aboutus` - Company information
* **Services** `services` - Services offered by Nordtrom, such as Spa and Tailoring
* Theme Results `th` _deprecated_
* Outfit `o` _deprecated_

There are many others. Please update if other page types not shown here reach greater record volume.
