## Introduction
Breadcrumb is a site navigation tag managed by Search & Browse team to identify how a customer navigates through Nordstrom.com.

This is a breadcrumbs as seen on Nordstrom.com: <br> `Home / Women / Shoes / Sneakers & Athletic`

Digital Merchandising relies on breadcrumb data to understand what categories, recommendation strategies, search features customers view and shop products from the most.

For example, Nike’s Air Max 270 can be viewed via:
* Browse (women, men, kids, pop-in, designer, etc)
* Recommendations
* Search
* Site Entry via PDP (cold start)
* etc.

> Important: Breadcrumbs are **not** related to [RMS](/w/methodology/merchandising-data.html#rms-hierarchy). <br>
> RMS maps where a product lives in the product hierarchy (Division, Sub Division, Supplier, etc) and completely unrelated to site navigation.

## Breadcrumb vs Category
The terms “breadcrumbs” and “categories” are often used interchangeably.<br>
From a business conversation perspective that’s **okay**.  From a site and data tagging perspective it is **not okay**.

* **Category** is the grouping of products of a certain type (aka category) for the customer to shop.
* **Breadcrumb** is the literal navigation trail of the category dictated by our site’s navigational tree setup in Compass.

Let’s take [Women’s Sneakers & Running Shoes](https://shop.nordstrom.com/c/womens-sneakers?origin=topnav&breadcrumb=Home%2fWomen%2fShoes%2fSneakers+%26+Athletic) for example:

Women’s Sneakers & Running Shoes is the **category**. <br>
Home / Women / Shoes / Sneakers & Athletic is the **breadcrumb**. <br>

![alt text](img/methodology-breadcrumb-category.png)

## Compass
Compass is Nordstroms current navigational mapping platform and what generates a breadcrumb. If you want to see our site navigation tree setup go to [Compass UI]()

_see Navigation Template: field specifies Sneakers & Athletic as category page_ <br>

### Platform Migration
The main confusion and the reason terms are interchanged has to do with Nordstrom’s navigational platform outputs, current and legacy:
* Compass (current)  `breadcrumbs`
* Site Manager (legacy)  `categoryid`

Search & Browse migrated navigation from Site Manager to Compass on June 10th, 2018. Migration was rolled out in phase, leaving Brands and Pop-In as final pieces. This migration meant the transition from Category ID to Breadcrumb.

```mermaid
gantt
    title Category ID to Breadcrumb Transition
    dateFormat  YYYY-MM-DD
    Category ID : a1, 2018-04-01, 2018-06-10
    Breadcrumbs : a2, 2018-06-10 , 2019-02-10
		Unstable Q4 : a3, 2018-11-01 , 2019-01-10
		Stable FY '19: after a3, 30d
```

> Unstable breadcrumbs in FY 2018 Q4 - site was firing Site Manager breadcrumbs instead of Compass generated breadcrumbs. It was nearly impossible to distinguish between the two and therefore deemed unreliable for reporting purposes. In addition, SPA complicated our ability to report on entry to PDP when referral URL's went static. These issues subsided mid-January 2019.

Dates of transition and what reports to use for a given period.

|Report|Method|Reporting Period|Source|
|--|--|--|--|
|Breadcrumb Funnel | Breadcrumb | FY 2019 forward | Snowplow|
|Product Breadcrumb| Url |6/10/2018 - 2/2/2019| Snowplow|
|Product Category| Category ID | Before 6/10/2018| Snowplow|
|Product Category| Category ID |2017 or earlier| Coremetrics|


## Navigation Type
Each breadcrumb is delimited by `/` to create hierarchical tiers. Navigation Type is derived from the first part of a breadcrumb and cart `placement_type` tags.

> `Home` encompasses Search & Browse. For reporting purposes we want them to be two separate line items. For this reason we use the second tier of the breadcrumb to derive Search.

|Navigation Type|Navigated to PDP via| Sample |
|---------| -----------| ---------|
| Browse | Top or Left navigation categories | `Home`  without `All Results` |
| Search | Search results page | `Home / All Results` |
| Recs | Recommendation trays  | `recs`|
|Site Entry via PDP| First site visit is on product page, aka cold start | `site entry via product page` |
| Looks | Add to Bag from Looks, Your Look | _cart tag_ `dynamicLooks`|
| Wishlist | Add to Bag from Wishlist in Secure | _cart tag_  `wishlist`
| Unknown | No breadcrumb or cart placement tag detail |  | |

## Breadcrumb Funnel
Breadcrumb funnel begins at a product page view level. In order to track a customers path through the funnel we start with `product_view` tags.

Product Detail Pages (PDP) are **not** the only place where a customer can view product and add to bag. However, it is the only place where a `product_view` tag will fire.

This poses two problems to the funnel:
1. If an `add_to_bag` occurs from Your Look or Wishlist, for example, a `product_view`  **tag never fires** and
2. link is missing from product view to add to bag

### Looks and Wishlist
With new site features, such as Looks, Your Look,  and Wishlist, where users can add products to bag has expanded. These features allow users to view a product without opening  a full PDP.

In order to track this activity in the funnel, we must  add them after all product view activity has been mapped to add to bag data.

Any `add_to_bag` records remaining will use Looks and Wishlist `placement_type` tags to assign Navigation Type - the only two not natively derived from breadcrumbs.

![looks](img/methodology-breadcrumb-looks.png)
_to open a PDP from Your Look user must click on “see full details” link_

**Wishlist**
![wishlist](img/methodology-breadcrumb-wishlist.png)
_to open a PDP from Wishlist user must click on “see details” link_

## Breadcrumb Demand Attribution
If there are gaps in the tags and/or actions occur on different days we lose the ability to map to Navigation Type and the attribution results are Unknown, default.

In the following scenarios, we will assume the customer landed on PDP from Search results page:
1. Viewed product; No add to bag
2. Viewed product AND added to bag
3. Add to bag from Looks in PDP _(no product view tag fired)_

```mermaid
graph TD
    B1[Product View Only]
    B1 -->|viewed| C1[fa:fa-eye Product Page]
    C1 .->D1((Search))
    
    B[Product View and Add to Bag]
    B -->|viewed| C[fa:fa-eye Product Page]
    C -->|added| D[fa:fa-shopping-bag Add to bag]
    D -->|ordered| E[fa:fa-check-circle Order]
    E .-> |Same Day Orders| F((Search))
    E .-> |Non-Same Day Orders| G((Unknown))
    
    
    B2[No Product View and Add to Bag]
    B2 .->|e.g. viewed from Looks in PDP| C2>fa:fa-ban No Product View]
    C2 .->|added| D2[fa:fa-shopping-bag Add to bag]
    click C2 """This is a tooltip for a link"
    D2 -->|ordered| E2[fa:fa-check-circle Order]
    
    E2 --> F2((Looks))
```