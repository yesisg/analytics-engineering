---
layout: wiki
title: CA Redshift
toc: true
category: datasources
tags: redshift dsa clickstream snowplow
---

## Introduction

CA Redshift is the Customer Analytics Database in [Redshift](https://aws.amazon.com/redshift/), which focuses primarily on clickstream information and purchasing history. This is used to drive most of the reporting that Analytics does.

![Redshift Logo]({{ "assets/images/datasources-ca-redshift-logo.png" | absolute_url }})


#### Configuring Your Connection

Once you're done configuring, you should follow these additional steps to ensure that you don't accidentally cause problems.

* **Autocommit** - You must turn on autocommit, or it will affect database performance for everyone.
* **Query Groups** - [There are three query groups](#workload-management-queues-wlm) you will want to use depending on the size of your query.


## Workload Management Queues (WLM)

Redshift uses query queues in order to make sure that certain queries don't get an overly large share of the computing resources avialable.

### Using a Queue

If you want to select a specific queue, you can execute this command (query group values are case-sensitive):

```sql
set query_group to '<query_group>';
```

There are 4 queues: the default, and then 3 specifically defined queues.

- `default` - This is your queue if you haven't specifically set one.
- `QG_S` - Small queue, for quick-running queries. 7 query slots.
- `QG_SM` - Medium queue, for slightly longer queries. 5 query slots.
- `QG_SML` - Large queue, for your most compute-intensive queries. 2 query slots.

### Checking Queues

You may need to figure out which queue you want to join prior to useing one. You can check how queues are being used at the moment like so:

```sql
select * from sys_mgmt.wlm_queue_state_vw order by description, slots
```
## Functions

In order to speed up some of the common tasks done in SQL, user-defined functions (UDFs) can be written in Redshift. Keeping with our implementation of Luigi, for our purposes most of these are written in python.


### Creating UDF

**Resources:**
- AWS - [Python Language Support for UDFs](https://docs.aws.amazon.com/redshift/latest/dg/udf-python-language-support.html)
- AWS - [CREATE FUNCTION documentation](https://docs.aws.amazon.com/redshift/latest/dg/r_CREATE_FUNCTION.html)
- [Scalar UDF Examples](https://github.com/awslabs/amazon-redshift-udfs/tree/master/scalar-udfs)

**Things to keep in mind:**

- Follow function naming best practices ([AWS Naming UDFs](https://docs.aws.amazon.com/redshift/latest/dg/udf-naming-udfs.html)). Always prefix any user-defined functions with `f_`, e.g. `f_get_query_param()`.
- Calling UDFs requires a schema at the front. For the ones defined here, that'll always be `bi_prd`.
- You need to grant access for others to use these functions before they can be implemented.
- Using any functions outside python standard library is likely more effort than it's worth, but you can view [how to do that here](https://docs.aws.amazon.com/redshift/latest/dg/udf-python-language-support.html).