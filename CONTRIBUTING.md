## Contributing

Unlike analysis- or user-specific information design, the types of processes that are contributed to this library are generally held to a higher standard for performance, understandability, maintainability, etc. This includes but is not limited to a few key tenets:

* **Naming Schemes** - Table, column and process names should simple, semantic and discoverable. Within reason, users should be able to tell what something contains by the name of it.
* **Performance** - Productionalized queries are expected to run efficiently, utilizing Redshift's unique architecture.
* **Maintainability** - Explanatory comments, links to documentation, and notes for later refactoring/recreating are required.
* **Consistency** - Columns should be the same name and data type across tables.
* **Idempotence** - Given the same input, re-running an ETL should create the exact same output everytime.
* **Code Style** - Queries are encouraged to be formatted in a clear, consistent manner.

<br><br>

----

<br><br>

## Production Query Guidelines
Follow these guidelines to create valid queries for running in prod. Queries written here must adhere to the [Production Naming Scheme conventions]() defined on the wiki.

|**Subject Area**|**Description**|
|----------------|---------------|
|Comments|All queries should have explanatory comments in them.<br>Inline comments where derived values or calculations are not obvious.<br>Header comments both in the top of the query, and above each section, describing the implementation and the purpose. Header comments should be written using block comment (i.e. `/* comment */` syntax.|
|Table Naming|All production tables should be created in the `bi_prd` schema. <br>Tables created in the `bi_` schemas should contain semantic names with no suffixes or prefixes (if possible).<br>Table name should indicate what they contain and, if applicable, the level of granularity at which they exist. Tables should be discoverable by their name alone.<br>For more information, check out [the Table Naming section of the production style guide.]()|
|Select Access|In order to allow others to select from your table, you need ot make sure to allow select access on that table.<br>`GRANT SELECT on <new_table_name> to PUBLIC;`|
|Incremental ETL Processes|Queries should be incremental where appropriate. Commonly, queries are restricted to a rolling 14-day window, as this is generally the time period where Snowplow data stabilizes. Always make sure to include a record timestamp for any newly inserted records.|
|File Type and Naming|All queries should be saved as `.sql` files.<br>File names should be the same as the table they affect (e.g. `bopus_daily.sql` updates the `bi_prd.bopus_daily` table).|
|One Output Per Query|Do not write more than one of the following into a single query:<br>`CREATE TABLE`<br>`INSERT`<br>`DELETE`<br>`UPDATE`<br>If you need to do multiple of those actions, split them into separate files. If they are not split and one of them fails, none of the transaction will be committed. If they need to be run in a specific order, contribute those to the _prioritized queries_ folder and describe the correct order they should be run in the merge request. If you absolutely do need to have multiple writes in one query, you should include a `COMMIT;` after each one.|
|Table Ownership & Existence|Tables that are regularly inserted into should be created under the ownership of the service account. If you have an insert statement that runs on a regular basis, you should also include a "Create Table" query for table creation under the _table definitions_ folder. `CREATE TABLE IF NOT EXISTS` is useful for those.|
|Redshift Table Best Practices|You should include a `sortkey` and a `distkey` in your table and follow general [best practices for table creation](http://docs.aws.amazon.com/redshift/latest/dg/t_Creating_tables.html). If you have a query that inserts values into a specific, custom table, you should include the corresponding `CREATE TABLE` query in the `table_definitions` folder.|
|Table Compression|Correct compression encodings should be applied to columns, using the settings returned by creating a test table and running `ANALYZE COMPRESSION` on it. Examples are available in the `table_definitions` folder.|
|Schedules|By default, queries run every night. If there's a reason your query should not, let us know and we can move it to a different schedule.|
|Inserts|Any insert should be written as a [merge statement](http://docs.aws.amazon.com/redshift/latest/dg/merge-examples.html), so that rows are not duplicated if jobs need to be re-run (which is often). [Examples are available on the Wiki]() of how to write a merge insert statement.|
|Code Style|- `snake_case` is preferable to `camelCase`<br>- SQL keywords should be in UPPERCASE<br>- Column and table identifiers should be lowercase<br>- Following [SQL Style Guide](http://www.sqlstyle.guide/) is encouraged!|

<br><br>

----

<br><br>

## Adding Queries to Production

In order to add a query to run on a nightly basis:

1. Add the query to the `queries_prioritized` folder.
2. Create a task class in `queries.py`.
    1. Subclass `PrioritizedQuery` class and override the `query_file` parameter with the query filename.
    2. Define the dependencies by overriding the `requires()` method.
    3. Subclass `BlockingDataCheck` for any tables that need to be tested for validity prior to the query running.
3. Add the task class to RunPrioritizedQueries class.
4. Edit the `config.yaml` file.
    1. Add an entry under `production_tables` for the new table.
    2. Add the correct `date_column`.
    3. Include true/false for `regression_test` if we want to test this on a nightly basis. If yes, add a value under `regression_days_back` for how many days back we want to check for data.
    4. Add true/false for `analyze_daily` if we want to include in the daily analyze queue.
    5. Add true/false for `vacuum` to indicate if this should be vacuumed on weekends.

Manual changes & testing required - this will provide the table state required for the query to run.

- Create any tables using the table creation statements provided by the user.
- Insert any backfills that the user has done into the production table.
- Run the query end-to-end once to verify that nothing errors out.

Create a merge request for `release/production`:

> Admins only

- Merge the query merge request into `master`, and make a new MR for `release/production`.
- Once the request is merged, verify that the docker image built correctly in Jenkins with no errors.
- If possible, run the specific tasks in luigi to verify that they work.

<br><br>