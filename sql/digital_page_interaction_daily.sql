/*
  DIGITAL PAGE INTERACTION
  (formerly Liveview)

  Tables:
  bi_dev.digital_page_interaction_staging
  clk_strm_sp.sp_engagements
  clk_strm_sp.sp_orders

*/

/*
=======================
Date Table
=======================
*/
DROP TABLE IF EXISTS dpi_date_range;
--[[bi_prd.digital_page_interaction]]
CREATE TEMPORARY TABLE dpi_date_range AS (
    SELECT
      cast(trunc(convert_timezone('PST8PDT', getdate()))- 15 AS TIMESTAMP) AS etlstart --etl starttmstp
    , trunc(convert_timezone('PST8PDT', getdate())) - 15 AS derivedstart --derived_date start
    , trunc(convert_timezone('PST8PDT', getdate())) - 1 AS derivedend --derived_date end
)
;

/*
=======================
Site Area Detail
- [note1] only return main category page w/o filtered details
=======================
*/
DROP TABLE IF EXISTS page_interaction_insert;
CREATE TEMPORARY TABLE page_interaction_insert
sortkey(activity_date)
AS
 
SELECT
        a.derived_date AS activity_date
      , dates.yr_idnt AS fiscal_year
      , dates.qtr_num AS fiscal_quarter
      , dates.half_num AS fiscal_half
      , dates.mth_num AS fiscal_month
      , dates.wk_num AS fiscal_week
      , dates.day_num AS fiscal_day
      , dates.day_idnt
      , dates.day_idnt_ly
      , dates.day_idnt_ly_realigned
      , dates.mth_desc AS fiscal_month_name
      , dates.wk_start_day_dt AS wk_start_date
      , dates.wk_end_day_dt AS wk_end_date
      , a.app_id
      , CASE WHEN mkt_channel IS NOT NULL THEN 'MARKETING' ELSE refr_page_type END refr_page_type
      , CASE WHEN mkt_source IS NOT NULL THEN mkt_source
              WHEN refr_page_type = 'SECURE' THEN split_part(a.refr_urlpath, '/', 2)
              WHEN refr_page_type IN ('CATEGORIES', 'CONTENT') THEN a.refr_urlpath
              ELSE refr_page_type END refr_page_detail
      , page_type
      , replace(a.page_urlpath, '/content/', '/c/') AS page_urlpath
      , target_page_type
      , CASE --PAGE SECTION
          WHEN is_engagement                    IS NOT NULL                       THEN 'ENGAGEMENTS'
          -- GLOBAL NAV
          WHEN target_origin                    = 'header-top-promo'              THEN 'GLOBAL NAV'
          WHEN target_page_type                 = 'SECURE'                        THEN 'GLOBAL NAV'
          WHEN target_origin                    = 'tab'                           THEN 'GLOBAL NAV'
          WHEN target_urlpath                   = '/'                             THEN 'GLOBAL NAV'
          -- SEARCH
          WHEN target_urlpath                   = '/sr'                           THEN 'SEARCH'
          WHEN search_keyword                   IS NOT NULL                       THEN 'SEARCH'
          WHEN target_origin                    = 'keywordsearch'                 THEN 'SEARCH'
          WHEN target_origin                    = 'predictivesearchproducts'      THEN 'SEARCH'
          WHEN target_origin                    LIKE 'recentsearches%'            THEN 'SEARCH'
          -- RECS           
          WHEN target_origin                    = 'coordinating'                  THEN 'RECS'
          WHEN target_origin                    = 'related'                       THEN 'RECS'
          WHEN target_recs_strategy             IS NOT NULL                       THEN 'RECS'
          -- SHOPPABLE  
          WHEN target_placement                 = 'str'                           THEN 'SHOPPABLE'
          WHEN target_page_type                 = 'PRODUCT'                       THEN 'PRODUCT'
          WHEN target_origin                    = 'category'                      THEN 'PRODUCT'
          WHEN target_page_type                 = 'LOOKS'                         THEN 'LOOKS'
          WHEN target_origin                    = 'productbrandlink'              THEN 'PRODUCT BRAND LINK'
          -- NAV
          WHEN target_origin                    = 'topnav'                        THEN 'TOP NAV'
          WHEN split_part(cm_sp_link, '_', 3)   = 'persnav'                       THEN 'TOP NAV'
          WHEN target_origin                    = 'leftnav'                       THEN 'LEFT NAV'
          WHEN target_breadcrumb                IS NOT NULL                       THEN 'LEFT NAV'
          WHEN target_origin                    = 'breadcrumb'                    THEN 'BREADCRUMB'
          -- GLOBAL FOOTER  
          WHEN target_origin                    <> 'footer' 
           AND split_part(cm_sp_link, '_', 1)   = 'globalfooter'                  THEN 'GLOBAL FOOTER'
          -- FOOTER 
          WHEN target_origin                    = 'footer'                        THEN 'FOOTER'
          WHEN split_part(cm_sp_link, '_', 1)   = 'globalfooternav'               THEN 'FOOTER'
          WHEN target_origin                    = 'tab-logo#back-to-top'          THEN 'FOOTER'
          WHEN target_urlpath                   = '/portalserver/nordstrom/index' THEN 'FOOTER'
          -- CONTENT
          WHEN cm_sp_full                       IS NOT NULL                       THEN 'CONTENT'
          -- MISC
          WHEN a.page_urlpath                   = target_urlpath                  THEN 'REFRESH'
          ELSE 'OTHER'
          END AS page_section
        , CASE --PAGE SECTION DETAIL
           WHEN is_engagement                     IS NOT NULL                                                  THEN 'FILTER/SORT/PAGINATION'
           WHEN target_page_type                  = 'SECURE'                                                THEN target_urlpath
           WHEN target_recs_placement             IN ('hp_ftr', 'ftr', 'hp_mobi_ftr', 'mobi_ftr')           THEN 'Global Recs'
           WHEN target_recs_page                  IN ('hp', 'hp1')                                          THEN 'Inline Recs'
           WHEN target_recs_page                  IN ('category', 'product')                                THEN target_recs_placement
           WHEN target_placement                  = 'str' AND shoppable_position IS NOT NULL                THEN shoppable_position
           WHEN target_recs_strategy              IS NOT NULL                                               THEN target_recs_strategy
           WHEN target_origin                     = 'pdp_easynav'                                           THEN 'PDP Easy Nav'
           WHEN target_page_type                  = 'PRODUCT'                                               THEN 'Product'
           WHEN target_origin                     = 'predictivesearchproducts'                              THEN 'PredictiveSearchProducts'
           WHEN target_origin                     = 'keywordsearch'                                         THEN 'Keyword'
           WHEN target_origin                     LIKE 'recentsearches%'                                    THEN 'Recentsearches'
           WHEN target_page_type                  = 'LOOKS'                                                 THEN coalesce(split_part(target_urlpath, '/', 3), '/yourlook')
           WHEN target_page_type                  = 'SEARCH'                                                THEN 'SEARCH'
           WHEN target_urlpath                    = '/'                                                     THEN 'HOMEPAGE'
           WHEN split_part(cm_sp_link, '_', 3)    = 'persnav'                                               THEN 'persnav'
           WHEN split_part(a.page_urlpath, '/', 5)  = 'write-review'                                        THEN 'write-review'
           WHEN target_urlpath                    = '/portalserver/nordstrom/index'                         THEN 'Pay Bill Online'
           WHEN target_page_type                  = 'FILTERED CATEGORIES'                                   THEN 'FILTERED CATEGORIES'
           WHEN target_page_type                  = 'FILTERED BRANDS'                                       THEN 'FILTERED BRANDS'
           ELSE target_urlpath
          END AS page_section_detail

        , count(*) clicks
        , sum(c.orders) orders
        , sum(c.demand) demand
        , count(DISTINCT a.domain_userid) visitors
        , count(DISTINCT a.domain_sessionid) sessions

    FROM bi_dev.link_clicks_staging a


    LEFT JOIN
      ( -- ENGAGEMENTS
    SELECT
      derived_date
      , app_id
      , domain_userid
      , domain_sessionid
      , page_urlpath
      , lower(category) AS category
      , lower(action) AS action
      , lower(label) AS label
      , derived_tstamp

    FROM clk_strm_sp.sp_engagement

    WHERE  --date filter
              etl_tstamp >= (SELECT etlstart FROM dpi_date_range)
        AND derived_date >= (SELECT derivedstart FROM dpi_date_range)
        AND derived_date <= (SELECT derivedend FROM dpi_date_range)
        AND app_id IN ('nord.com', 'n.com', 'nord.mow', 'nord.ios', 'nord.android')
        AND domain_userid <> ''
        AND domain_userid IS NOT NULL
    ) b
      ON a.derived_date = b.derived_date
      AND a.app_id = b.app_id
      AND a.domain_userid = b.domain_userid
      AND a.domain_sessionid = b.domain_sessionid
      AND a.target_urlpath = b.page_urlpath
      AND a.click_tstamp = b.derived_tstamp


  LEFT JOIN
    ( -- ORDERS
    SELECT DISTINCT
        derived_date,
        domain_userid,
        domain_sessionid,
        app_id,
        max(derived_tstamp) AS order_tstamp,
        sum(tr_total) AS demand,
        count(tr_orderid) AS orders

    FROM
    clk_strm_sp.sp_orders ord

    WHERE --date filter
              etl_tstamp >= (SELECT etlstart FROM dpi_date_range)
        AND derived_date >= (SELECT derivedstart FROM dpi_date_range)
        AND derived_date <= (SELECT derivedend FROM dpi_date_range)
        -- other filters
        AND app_id IN ('nord.com', 'n.com', 'nord.mow', 'nord.ios', 'nord.android')
        AND domain_userid <> ''
        AND domain_userid IS NOT NULL
    GROUP BY 1, 2, 3, 4
    ) c
      ON a.derived_date = c.derived_date
      AND a.app_id = c.app_id
      AND a.domain_userid = c.domain_userid
      AND a.domain_sessionid = c.domain_sessionid
      AND a.click_tstamp <= c.order_tstamp

    LEFT JOIN bi_prd.dates dates
      ON a.derived_date = dates.day_dt

    WHERE --date filter
          a.derived_date >= (SELECT derivedstart FROM dpi_date_range)
      AND a.derived_date <= (SELECT derivedend FROM dpi_date_range)


  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
;

/*
--------------------------
  MERGE
--------------------------
*/
DELETE FROM bi_prd.digital_page_interaction_daily
WHERE
    bi_prd.digital_page_interaction_daily.activity_date >= (SELECT derivedstart FROM dpi_date_range)
AND bi_prd.digital_page_interaction_daily.activity_date <=  (SELECT derivedend FROM dpi_date_range)
;


/*
--------------------------
  INSERT
--------------------------
*/
INSERT INTO bi_prd.digital_page_interaction_daily
SELECT
    --dates & app
     activity_date
  , fiscal_year
  , fiscal_quarter
  , fiscal_half
  , fiscal_month
  , fiscal_week
  , fiscal_day
  , day_idnt
  , day_idnt_ly
  , day_idnt_ly_realigned
  , fiscal_month_name
  , wk_start_date
  , wk_end_date
  , app_id
  -- path
  , refr_page_type
  , refr_page_detail
  , page_type
  , page_urlpath
  , target_page_type
  , page_section
  , page_section_detail
  -- metrics
  , sum(clicks) clicks
  , sum(orders) orders
  , sum(demand) demand
  , sum(visitors) visitors
  , sum(sessions) sessions

FROM page_interaction_insert

GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
  ;