/*
---------------------------------------------------------------
navigation Traffic Daily
Details how a customer arrived at a Product Page using navigation data (Search & Browse).

Digital Merch uses navigation traffic to understand what visitor traffic via categories, recommendation strategy,
search feature customers view and shop products from the most. Not related to RMS hierarchy.

Should only be backfilled to beginning of FY 2019 (2019-02-03)

Created by: Yesi Garcia
Created on: 3/2019
---------------------------------------------------------------
*/


/*
---------------------------------------------------------------
   Temp table declaring DATE variables throughout stack
   Should only be backfilled to beginning of FY 2019 (2019-02-03)
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS navigation_traffic_date_range;
--[[bi_prd.navigation_traffic_daily]]
CREATE TEMPORARY TABLE navigation_traffic_date_range AS (
    SELECT
     trunc(convert_timezone('PST8PDT', GETDATE())) - 15 AS derivedstart --derived_date start
   , trunc(convert_timezone('PST8PDT', GETDATE())) - 1 AS derivedend --derived_date end
   --manual
   -- CAST('2019-03-03' AS DATE) AS derivedstart
   -- , CAST('2019-03-05' AS DATE) AS derivedend
);



/*
------------------------------
Get product views and instock at the cookie/day/platform/navigation level
------------------------------
*/
DROP TABLE IF EXISTS navigation_traffic_product_views;
CREATE TEMPORARY TABLE navigation_traffic_product_views
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(activity_date)
AS
(
SELECT
    activity_date
    , domain_userid
    , navigation
    , app_id
    , SUM(product_views) AS product_views
    , SUM(rp_instock_views) + SUM(nrp_instock_views) AS instock_views

FROM
  ( --Aggregate to style group so we can join our instock data
    SELECT
        a.activity_date
        , a.domain_userid
        , a.style_group_idnt
        , a.navigation
        , a.app_id
        , COUNT(a.domain_sessionid) AS product_views
        , COUNT(a.domain_sessionid) * AVG(i.rp_stock_share) AS rp_instock_views
        , COUNT(a.domain_sessionid) * AVG(i.nrp_stock_share) AS nrp_instock_views

    FROM
        ( --product views
        SELECT
            derived_date as activity_date
            , style_number AS product_id
            , split_part(style_number, '_', 1) AS style_group_idnt
            , domain_userid
            , domain_sessionid
            , app_id
            , navigation

        FROM clk_strm_sp.sp_product_views

        WHERE
            derived_date >= (SELECT derivedstart FROM navigation_traffic_date_range)
            AND derived_date <= (SELECT derivedend FROM navigation_traffic_date_range)
            AND app_id IN ('nord.com', 'nord.mow', 'nord.ios', 'nord.android')
        ) a
        LEFT JOIN bi_prd.instock_daily i
            ON a.activity_date = i.activity_date
            AND a.style_group_idnt = i.style_group_idnt

    GROUP BY 1, 2, 3, 4, 5
  ) a

GROUP BY 1, 2, 3, 4
);

/*
------------------------------
Get add-to-bag at the cookie/day/platform/navigation level
------------------------------
*/
DROP TABLE IF EXISTS navigation_traffic_cart_adds;
CREATE TEMPORARY TABLE navigation_traffic_cart_adds
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(activity_date)
AS
(
SELECT
    derived_date as activity_date
    , domain_userid
    , navigation
    , app_id
    , SUM(quantity) AS cart_adds

FROM clk_strm_sp.sp_cart

WHERE
    derived_date >= (SELECT derivedstart FROM navigation_traffic_date_range)
    AND derived_date <= (SELECT derivedend FROM navigation_traffic_date_range)
    AND app_id IN ('nord.com', 'nord.mow', 'nord.ios', 'nord.android')
    AND event_name = 'add_to_cart'
    AND type = 'add'

    GROUP BY 1, 2, 3, 4
);

/*
------------------------------
Get demand at the cookie/day/platform/navigation/order level
------------------------------
*/
DROP TABLE IF EXISTS navigation_traffic_orders;
CREATE TEMPORARY TABLE navigation_traffic_orders
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(activity_date)
AS
(
SELECT
    derived_date as activity_date
    , domain_userid
    , navigation
    , app_id
    , COUNT(DISTINCT ti_orderid) AS orders
    , SUM(ti_price * ti_quantity) AS demand
    , SUM(ti_quantity) AS order_units

FROM clk_strm_sp.sp_order_items

WHERE
    derived_date >= (SELECT derivedstart FROM navigation_traffic_date_range)
    AND derived_date <= (SELECT derivedend FROM navigation_traffic_date_range)
    AND app_id IN ('nord.com', 'nord.mow', 'nord.ios', 'nord.android')

GROUP BY 1, 2, 3, 4
HAVING SUM(ti_price * ti_quantity) > 0
);


/*
------------------------------
Get page views by date/cookie/URL/platform/navigation for the categories
we care about - browse and brands
------------------------------
*/
DROP TABLE IF EXISTS navigation_traffic_page_views;

CREATE TEMPORARY TABLE navigation_traffic_page_views
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(activity_date)
AS
(
SELECT
    derived_date as activity_date
    , domain_userid
    , app_id
    , navigation
    , COUNT(domain_sessionid) AS page_views

FROM clk_strm_sp.sp_page_views

WHERE
    derived_date >= (SELECT derivedstart FROM navigation_traffic_date_range)
    AND derived_date <= (SELECT derivedend FROM navigation_traffic_date_range)
    AND app_id IN ('nord.com', 'nord.mow', 'nord.ios', 'nord.android')

GROUP BY 1, 2, 3, 4
);

/*
------------------------------
Merge everything together at the category/cookie/day level
------------------------------
*/
DROP TABLE IF EXISTS navigation_traffic_merge_summary;
CREATE TEMPORARY TABLE navigation_traffic_merge_summary
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(activity_date)
AS
(
    SELECT
        sub.activity_date
        , sub.domain_userid
        , sub.navigation
        , sub.app_id
        -- , page.page_views AS page_views
        -- , page.entry_page_views AS entry_page_views
        -- , page.one_page_visitors AS one_page_visitors
        , prdct.product_views
        , prdct.instock_views
        , prdct.domain_userid AS viewing_userid
        , add.cart_adds
        , add.domain_userid AS add_userid
        , dmnd.domain_userid AS buying_userid
        , dmnd.order_units
        , dmnd.demand
        , dmnd.orders

    FROM
        (
                SELECT
                    activity_date
                    , domain_userid
                    , navigation
                    , app_id

                FROM navigation_traffic_page_views

            UNION

                SELECT
                    activity_date
                    , domain_userid
                    , navigation
                    , app_id

                FROM navigation_traffic_product_views

            UNION

                SELECT
                    activity_date
                    , domain_userid
                    , navigation
                    , app_id

                FROM navigation_traffic_cart_adds

            UNION

                SELECT
                    activity_date
                    , domain_userid
                    , navigation
                    , app_id

                FROM navigation_traffic_orders
        ) sub
        -- LEFT JOIN bc_traf_pgvw_merge page
        --     ON sub.activity_date = page.derived_date
        --     AND sub.domain_userid = page.domain_userid
        --     AND sub.navigation = page.navigation
        --     AND sub.app_id = page.app_id
        LEFT JOIN navigation_traffic_product_views prdct
            ON sub.activity_date = prdct.activity_date
            AND sub.domain_userid = prdct.domain_userid
            AND sub.navigation = prdct.navigation
            AND sub.app_id = prdct.app_id
        LEFT JOIN navigation_traffic_cart_adds add
            ON sub.activity_date = add.activity_date
            AND sub.domain_userid = add.domain_userid
            AND sub.navigation = add.navigation
            AND sub.app_id = add.app_id
        LEFT JOIN navigation_traffic_orders dmnd
            ON sub.activity_date = dmnd.activity_date
            AND sub.domain_userid = dmnd.domain_userid
            AND sub.navigation = dmnd.navigation
            AND sub.app_id = dmnd.app_id
);

/*
---------------------------------------------------------------
DELETE
remove data from dynamic date range
---------------------------------------------------------------
*/
DELETE FROM bi_prd.navigation_traffic_daily
WHERE activity_date >= (SELECT derivedstart FROM navigation_traffic_date_range)
AND activity_date <= (SELECT derivedend FROM navigation_traffic_date_range)
;

/*
---------------------------------------------------------------
INSERT
  add new data from dynamic date range plus fiscal dates
---------------------------------------------------------------
*/

INSERT INTO bi_prd.navigation_traffic_daily
SELECT
    a.activity_date
    , dt.yr_num as fiscal_year
    , dt.half_num as fiscal_half
    , dt.qtr_num as fiscal_quarter
    , dt.mth_num as fiscal_month
    , dt.wk_num as fiscal_week
    , dt.day_num as fiscal_day
    , dt.day_idnt
    , dt.day_idnt_ly
    , dt.day_idnt_ly_realigned
    , a.domain_userid
    , a.navigation
    , a.app_id
    , NULL as page_urlpath
    , NULL AS page_views
    , NULL AS entry_page_views
    , NULL AS one_page_visitors
    , a.product_views
    , a.instock_views
    , a.viewing_userid
    , a.cart_adds
    , a.add_userid
    , a.buying_userid
    , a.order_units
    , a.demand
    , a.orders

FROM navigation_traffic_merge_summary a

JOIN bi_prd.dates dt ON a.activity_date = dt.day_dt
;
