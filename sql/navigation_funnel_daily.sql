/*
---------------------------------------------------------------
Breadcrumb Funnel Daily
Details how a customer arrived at a Product Page using breadcrumb data (Search & Browse).

Digital Merch uses breadcrumb funnel to understand what categories, recommendation strategy,
search feature customers view and shop products from the most. Not related to RMS hierarchy.

Should only be backfilled to beginning of FY 2019 (2019-02-03)

Created by: Yesi Garcia
Created on: 3/2019
---------------------------------------------------------------
*/

/*
---------------------------------------------------------------
   Temp table declaring DATE variables throughout stack
   Should only be backfilled to beginning of FY 2019 (2019-02-03)
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS pbc_funnel_date_range;
--[[bi_prd.navigation_funnel_daily]]
CREATE TEMPORARY TABLE pbc_funnel_date_range AS (
    SELECT
     trunc(convert_timezone('PST8PDT', GETDATE())) - 15 AS derivedstart --derived_date start
   , trunc(convert_timezone('PST8PDT', GETDATE())) - 1 AS derivedend --derived_date end
   -- manual
   -- CAST('2019-03-03' AS DATE) AS derivedstart
   --, CAST('2019-03-05' AS DATE) AS derivedend
);



/*
---------------------------------------------------------------
PRODUCT VIEWS
  view_rank is used to align a product view to an add to cart action
  is_site_entry is a safeguard for erroneous Site Manager breadcrumbs firing on cold start
  is_looks is a flag to identify a product was viewed from looks (erroneous breadcrumb firing otherwise)
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS navigation_funnel_product_views;
CREATE TEMPORARY TABLE navigation_funnel_product_views
diststyle KEY
distkey(domain_userid)
sortkey(style_number)
AS
(  SELECT
      a.derived_date
      , a.domain_userid
      , a.domain_sessionid
      , a.app_id
      , a.style_number
      , split_part(a.style_number, '_', 1) AS style_group_idnt
      , a.breadcrumb
      , a.derived_tstamp
      , CASE WHEN (lower(a.breadcrumb) IS NOT NULL AND lower(a.breadcrumb) <> 'site entry via product page') AND page_referrer IS NULL THEN 1 ELSE 0 END is_site_entry
      , CASE WHEN lower(a.breadcrumb) = 'site entry via product page' and split_part(refr_urlpath, '/', 2) IN ('look', 's') THEN 1 ELSE 0 END is_looks
      , ROW_NUMBER() OVER (PARTITION BY a.derived_date, a.domain_userid, a.style_number ORDER BY a.derived_tstamp) AS view_rank

FROM clk_strm_sp.sp_product_views a

WHERE a.derived_date >= (SELECT derivedstart FROM pbc_funnel_date_range)
AND a.derived_date <= (SELECT derivedend FROM pbc_funnel_date_range)
AND a.app_id IN ('nord.com', 'nord.mow') --'nord.ios', 'nord.android',
);

/*
---------------------------------------------------------------
CART ADDS
  - add_rank is used to align a add to cart product to a view
  - placement_type is used to provide more detail on prod view
    when breadcrumb is unavailable in sp_product_views
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS navigation_funnel_cart_adds;
CREATE TEMPORARY TABLE navigation_funnel_cart_adds
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(derived_date)
AS
(
  SELECT
       a.derived_date
      , a.app_id
      , a.domain_userid
      , a.domain_sessionid
      , a.style_number
      , split_part(a.style_number, '_', 1) AS style_group_idnt
      ,	a.breadcrumb
      , CASE WHEN lower(a.placement_type) IN ('lookpage', 'look page', 'dynamic looks') THEN 1 ELSE 0 END is_looks
      , CASE lower(a.placement_type)
          WHEN 'lookpage' THEN 'Looks/Your Looks'
          WHEN 'look page' THEN 'Looks/Your Looks'
          WHEN 'dynamic looks' THEN 'Looks/Dynamic Looks'
          WHEN 'inline results' THEN 'APP/Search'
          WHEN 'wishlist' THEN 'Wishlist'
          ELSE (CASE
                    WHEN a.app_id IN ('nord.ios', 'nord.android') AND lower(a.placement_type) = 'pdp'
                    THEN 'APP/Product Page' ELSE NULL
                END)
          END AS alt_breadcrumb
      , derived_tstamp
      , ROW_NUMBER() OVER (PARTITION BY a.derived_date, a.domain_userid, a.style_number ORDER BY derived_tstamp) AS add_rank
      , a.quantity AS cart_adds

  FROM clk_strm_sp.sp_cart a

  WHERE
      -- date filter
          a.derived_date >= (SELECT derivedstart FROM pbc_funnel_date_range)
      AND a.derived_date <= (SELECT derivedend FROM pbc_funnel_date_range)
      AND a.app_id IN ('nord.com', 'nord.mow')--'nord.ios', 'nord.android',
      AND a.event_name = 'add_to_cart'
      AND a.type = 'add'
);

/*
---------------------------------------------------------------
ORDERS
order_rank is used to align an order with product in add to cart
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS navigation_funnel_orders;
CREATE TEMPORARY TABLE navigation_funnel_orders
DISTSTYLE KEY
DISTKEY(style_number)
SORTKEY(derived_date)
AS
(
 SELECT
   a.derived_date
   , a.domain_userid
   , a.domain_sessionid
   , a.app_id
   , a.style_number
   , split_part(a.style_number, '_', 1) AS style_group_idnt
   , a.breadcrumb
   , a.order_rank
   , SUM(a.ti_price * a.ti_quantity) AS demand
   , COUNT(DISTINCT CASE WHEN orderid_rank = 1 THEN ti_orderid ELSE NULL END) orders
   , SUM(a.ti_quantity) AS order_units

 FROM
 	(
    SELECT
  		  derived_date
  		, derived_tstamp
	  	, app_id
	  	, domain_userid
      , domain_sessionid
	  	, style_number
	  	, breadcrumb
	  	, ti_quantity
	  	, ti_orderid
	  	, ti_price
	  	, ROW_NUMBER() OVER (PARTITION BY a.derived_date, a.domain_userid, a.ti_orderid, a.style_number ORDER BY derived_tstamp) orderid_rank
	  	, ROW_NUMBER() OVER (PARTITION BY a.derived_date, a.domain_userid, a.ti_orderid, a.style_number ORDER BY derived_tstamp) order_rank

  	FROM clk_strm_sp.sp_order_items a

  	WHERE -- date filter
  		    a.derived_date >= (SELECT derivedstart FROM pbc_funnel_date_range)
  		AND a.derived_date <= (SELECT derivedend FROM pbc_funnel_date_range)
  		AND a.app_id IN ('nord.com', 'nord.mow')--'nord.ios', 'nord.android',
	)a

  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8

  HAVING sum(a.ti_price * a.ti_quantity) > 0
);


/*
---------------------------------------------------------------
PRODUCT LEVEL AGGREGRATE
Include instock metrics in this step.

Breadcrumb is derived using cart values first and then product views to get max
coverage

Visit Type requires additional join in Tableau for up to date mapping:
Join sp_visit_type.mktg_channel to bi_usr.bt0x_marketing_channel_hierarchy
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS navigation_funnel_product_aggregate;
CREATE TEMPORARY TABLE navigation_funnel_product_aggregate
diststyle KEY
distkey(style_number)
sortkey(derived_date)
AS
  SELECT
     COALESCE(p.derived_date, c.derived_date, o.derived_date) 						          AS derived_date
   , COALESCE(p.app_id, c.app_id, o.app_id) 									                AS app_id
   , COALESCE(p.style_group_idnt, c.style_group_idnt, o.style_group_idnt) 				      AS style_group_idnt
   , COALESCE(p.style_number, c.style_number, o.style_number) 						          AS style_number
   , CASE WHEN p.is_looks = 1 OR c.is_looks = 1 THEN 1 ELSE 0 END   AS is_looks
   , p.is_site_entry
   , COALESCE(c.breadcrumb, p.breadcrumb, o.breadcrumb, 'Unknown') 					  AS breadcrumb
   , COALESCE(c.alt_breadcrumb, NULL)								                AS alt_breadcrumb
   , COALESCE(price.price_type, 'Unknown')                          AS price_type
   , COALESCE(COUNT(p.derived_tstamp), 0)								            AS product_views
   , COALESCE(SUM(c.cart_adds), 0) 										              AS cart_adds
   , COALESCE(SUM(o.orders), 0)                                     AS orders
   , COALESCE(SUM(o.order_units), 0)                                AS order_units
   , COALESCE(SUM(o.demand), 0.00)                                  AS demand
   , COALESCE(COUNT(p.derived_tstamp) * avg(i.rp_prod_share), 0) 		AS rp_views
   , COALESCE(COUNT(p.derived_tstamp) * avg(i.nrp_prod_share), 0) 	AS nrp_views
   , COALESCE(COUNT(p.derived_tstamp) * avg(i.rp_stock_share), 0) 	AS rp_instock_views
   , COALESCE(COUNT(p.derived_tstamp) * avg(i.nrp_stock_share), 0) 	AS nrp_instock_views

  FROM navigation_funnel_product_views p

  --CART ADD
  FULL OUTER JOIN navigation_funnel_cart_adds c
    ON p.derived_date = c.derived_date
    AND p.app_id = c.app_id
    AND p.domain_userid = c.domain_userid
    AND p.domain_sessionid = c.domain_sessionid
    AND p.style_number = c.style_number
    AND p.view_rank = c.add_rank

  --ORDERS
  FULL OUTER JOIN navigation_funnel_orders o
    	ON c.app_id = o.app_id
    	AND c.derived_date = o.derived_date
    	AND c.domain_userid = o.domain_userid
        AND c.domain_sessionid = o.domain_sessionid
    	AND c.style_number = o.style_number
    	AND c.style_group_idnt = o.style_group_idnt
    	AND c.add_rank = o.order_rank

  -- INSTOCK
  LEFT OUTER JOIN bi_prd.instock_daily i
      ON p.derived_date = i.activity_date
      AND p.style_group_idnt = i.style_group_idnt

  --PRICE TYPE
  LEFT OUTER JOIN bi_prd.price_type_daily price
     ON p.style_number = price.product_id
     AND p.derived_date = price.activity_date

-- -- LEFT JOIN navigation_funnel_visit_type v
-- --   ON p.domain_userid = v.domain_userid
-- --   AND p.derived_date = v.view_date
--
   GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9
;

/*
---------------------------------------------------------------
DELETE
remove data from dynamic date range
---------------------------------------------------------------
*/
  DELETE FROM bi_prd.navigation_funnel_daily
  WHERE bi_prd.navigation_funnel_daily.activity_date >= (SELECT derivedstart FROM pbc_funnel_date_range)
  AND bi_prd.navigation_funnel_daily.activity_date <= (SELECT derivedend FROM pbc_funnel_date_range)
  ;

/*
---------------------------------------------------------------
INSERT
  add new data from dynamic date range plus fiscal dates
---------------------------------------------------------------
*/

INSERT INTO bi_prd.navigation_funnel_daily
SELECT
    derived_date AS activity_date
    , dt.yr_num AS fiscal_year
    , dt.half_num AS fiscal_half
    , dt.qtr_num AS fiscal_quarter
    , dt.mth_num AS fiscal_month
    , dt.wk_num AS fiscal_week
    , dt.day_num AS fiscal_day
    , dt.day_idnt
    , dt.day_idnt_ly
    , dt.day_idnt_ly_realigned
    , app_id
    , style_group_idnt
    , style_number AS product_id
    , is_looks
    , is_site_entry
    , breadcrumb
    , alt_breadcrumb
    , price_type AS price_type
    --metrics
    , SUM(product_views) AS product_views
    , SUM(rp_views) AS rp_views
    , SUM(nrp_views) AS nrp_views
    , SUM(rp_instock_views) AS rp_instock_views
    , SUM(nrp_instock_views) AS nrp_instock_views
	, SUM(COALESCE(rp_instock_views, 0.00)) + SUM(COALESCE(nrp_instock_views, 0.00)) AS instock_views
    , SUM(cart_adds) AS cart_adds
    , SUM(orders) AS orders
    , SUM(order_units) AS order_units
    , SUM(demand) AS demand
    --update
    , trunc(convert_timezone('US/Pacific', getdate())) AS record_update_date

FROM navigation_funnel_product_aggregate main

LEFT JOIN bi_prd.dates dt
    ON main.derived_date = dt.day_dt

GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
;
