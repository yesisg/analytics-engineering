/*
  TOP NAVIGATION using breadcrumbs
  Match available breadcrumbs to page_urlpath.
  For all missing breadcrumbs, use site promotions > Promotions column.
*/


/*
---------------------------------------------------------------
   Temp table declaring DATE variables throughout stack
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS top_nav_date_range;
--[[bi_prd.top_navigation_daily]]
CREATE TEMPORARY TABLE top_nav_date_range AS (
    SELECT
      trunc(convert_timezone('PST8PDT', getdate())) - 15 AS derivedstart --derived_date start
    , trunc(convert_timezone('PST8PDT', getdate())) - 1 AS derivedend --derived_date end
);

--PAGE VIEWS
DROP TABLE IF EXISTS top_nav_groomed;
CREATE TEMPORARY TABLE top_nav_groomed
distkey(domain_userid)
sortkey(derived_date)

AS

SELECT
      app_id
    , derived_date
    , domain_userid
    , domain_sessionid
    , REPLACE(trim(page_urlpath), '/content/', '/c/') AS page_urlpath
    , breadcrumb
    , split_part(breadcrumb, '/', 1) AS tier_one
    , split_part(breadcrumb, '/', 2) AS tier_two
    , split_part(breadcrumb, '/', 3) AS tier_three
    , split_part(breadcrumb, '/', 4) AS tier_four
    , split_part(breadcrumb, '/', 5) AS tier_five

FROM
    (--groomed subquery

        SELECT DISTINCT
             a.app_id
             , a.derived_date
             , a.domain_userid
             , a.domain_sessionid
             , a.page_urlpath
             , page_url
             , replace(a.breadcrumb, '|', '/') AS breadcrumb

        FROM clk_strm_sp.sp_page_views a
        WHERE
            -- date filter
                a.derived_date >= (SELECT derivedstart FROM top_nav_date_range)
            AND a.derived_date <= (SELECT derivedend FROM top_nav_date_range)
            -- other filters
            AND ((bi_prd.f_get_urlquery_param(a.page_url, 'page')  IS NULL
              OR bi_prd.f_get_urlquery_param(a.page_url, 'page') = 1) -- remove pagination past page 1
            AND bi_prd.f_get_urlquery_param(a.page_url, 'top')    IS NULL  --remove filter
            AND bi_prd.f_get_urlquery_param(a.page_url, 'sort')   IS NULL  --remove sort
            AND bi_prd.f_get_urlquery_param(a.page_url, 'offset') IS NULL  --remove filter
            AND bi_prd.f_get_urlquery_param(a.page_url, 'cm_mmc') IS NULL)  --remove marketing

            -- only keep Top Nav
            AND lower(bi_prd.f_get_urlquery_param(a.page_url, 'origin')) = 'topnav' --page_views
            AND lower(split_part(a.page_urlpath, '/', 2)) IN ('c', 'content', 'choose-three-free-samples')  -- only keep category and content pages
    )
;


/*
------------------------------------------------------------
 TOP NAV CLICKS
 Joined to sp_page_views to retrieve breadcrumb information
 Removes all urls with filter and/or marketing parameters
 When breadcrumb not available, use Site Promo Promotion tag

Breadcrumb levels:
H1 - always Home
H2 - Parent Category or DLP
H3 - View ALL or Headers (bold on site drop down)
H4 - Featured and/or Categories
H5 - Sub Categories
 ------------------------------------------------------------
*/

DROP TABLE IF EXISTS top_nav_insert;
CREATE TEMPORARY TABLE top_nav_insert
sortkey(activity_date)
AS

SELECT
      a.derived_date             AS activity_date
    , dates.yr_num                AS fiscal_year
    , dates.qtr_num               AS fiscal_quarter
    , dates.half_num              AS fiscal_half
    , dates.mth_num               AS fiscal_month
    , dates.wk_num                AS fiscal_week
    , dates.day_num               AS fiscal_day
    , dates.day_idnt              AS day_idnt
    , dates.day_idnt_ly           AS day_idnt_ly
    , dates.day_idnt_ly_realigned AS day_idnt_ly_realigned
    , dates.wk_end_day_dt         AS wk_end_day_dt
    , a.app_id
    , a.page_urlpath
    , COALESCE(left(a.breadcrumb, 500), b.breadcrumb)  AS breadcrumb --if breadcrumb is null, return primary as-is
    , tier_one
    , tier_two
    , tier_three
    , tier_four
    , tier_five
    , COUNT(*) clicks

FROM top_nav_groomed a

LEFT JOIN bi_usr.primary_breadcrumb b --view
    ON replace(a.page_urlpath, '/content/', '/c/') = trim(b.uri)

LEFT JOIN bi_prd.dates AS dates
       ON derived_date = dates.day_dt

GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
;


/*
--------------------------
  MERGE
--------------------------
*/
DELETE FROM bi_prd.top_navigation_daily
WHERE
    bi_prd.top_navigation_daily.activity_date >= (SELECT derivedstart FROM top_nav_date_range)
AND bi_prd.top_navigation_daily.activity_date <=  (SELECT derivedend FROM top_nav_date_range)
;

/*
-------------------------------
Insert
-------------------------------
*/
INSERT INTO bi_prd.top_navigation_daily (
--Dates
    activity_date
    , fiscal_year
    , fiscal_quarter
    , fiscal_half
    , fiscal_month
    , fiscal_week
    , fiscal_day
    , day_idnt
    , day_idnt_ly
    , day_idnt_ly_realigned
    , wk_end_day_dt

-- report dimensions
    , app_id
    , page_urlpath
    , breadcrumb
    , tier_one
    , tier_two
    , tier_three
    , tier_four
    , tier_five
    , clicks

)

    SELECT
    --Dates
      activity_date
    , fiscal_year
    , fiscal_quarter
    , fiscal_half
    , fiscal_month
    , fiscal_week
    , fiscal_day
    , day_idnt
    , day_idnt_ly
    , day_idnt_ly_realigned
    , wk_end_day_dt

-- report dimensions
    , app_id
    , page_urlpath
    , breadcrumb
    , tier_one
    , tier_two
    , tier_three
    , tier_four
    , tier_five
    , clicks

FROM top_nav_insert
;
