/*
  Custom SQL for Tableau dashboard:
  DIVISIONAL DEMAND BY MARKETING CHANNEL

  Also powers self-service published data source of same name
*/
SELECT
    dt.wk_end_day_dt AS week_end_date
   , dt.wk_num_realigned AS fiscal_week
   , dt.mth_desc_realigned AS fiscal_month
   , dt.yr_idnt_realigned AS fiscal_year
   , dt.qtr_desc_realigned AS fiscal_quarter
   , dt.half_desc_realigned AS fiscal_half
   , sdc.div_no
   , sdc.div_desc
   , sdc.subdiv_no
   , sdc.subdiv_desc
   , sdc.dept_no
   , sdc.dept_desc
   , sdc.class_no
   , sdc.class_desc
   , sdc.supp_no
   , sdc.supp_name
   , sdc.brand
   , sdc.npg_ind
   , CASE WHEN sdc.npg_ind = 'Y' THEN 'NPG' ELSE COALESCE(npp.npp_tier, 'Non-NPP') END AS npp_tier
   , fct.price_type
   , fct.platform
   , mch.marketing_type AS channel_type
   , mch.finance_rollup AS marketing_channel
   , mch.marketing_channel_detailed AS program_summary
    , ty_ly_ind
    , SUM(fct.demand) AS web_demand
    , SUM(fct.com_demand) AS com_demand
    , SUM(fct.items_sold) AS items_sold
    , SUM(fct.product_views) AS product_views
    , SUM(fct.viewing_sessions) AS viewing_sessions
    , SUM(fct.buying_sessions) AS buying_sessions
    , SUM(fct.items_added) AS items_added
    , SUM(fct.in_stock_views) AS instock_views
    , SUM(fct.rp_views) AS rp_views
    , SUM(fct.nrp_views) AS nrp_views
    , SUM(fct.rp_instock_views) AS rp_instock_views
    , SUM(fct.nrp_instock_views) AS nrp_instock_views

FROM bi_prd.style_group_daily_funnel fct


LEFT JOIN
    ( --realigned dates
    SELECT
          true_day.day_dt
        , true_day.wk_end_day_dt
        , true_day.yr_idnt AS fiscal_year
        , realigned.yr_idnt - 1 AS yr_idnt_realigned
        , realigned.half_454 AS half_454_realigned
        , realigned.half_desc AS half_desc_realigned
        , realigned.qtr_454 AS qtr_454_realigned
        , realigned.qtr_desc AS qtr_desc_realigned
        , realigned.mth_454 AS mth_454_realigned
        , realigned.mth_desc AS mth_desc_realigned
        , realigned.wk_num AS wk_num_realigned
        , realigned.wk_desc AS wk_desc_realigned
        , realigned.day_num AS day_num_realigned

    FROM bi_prd.dates true_day

    LEFT JOIN bi_prd.dates realigned
        ON (true_day.day_idnt = realigned.day_idnt_ly_realigned)

    WHERE true_day.wk_end_day_dt <= (SELECT MAX(view_date) FROM bi_prd.style_group_daily_funnel fct)
      ) dt
    ON fct.view_date = dt.day_dt

INNER JOIN
    ( -- TY/LY Indicator
        SELECT
            fiscal_year
            , CASE
                WHEN yr_rank = 1 THEN 'TY'
                WHEN yr_rank = 2 THEN 'LY'
                ELSE 'OTHER' END AS ty_ly_ind
            , MIN(latest_wk) OVER (ORDER BY fiscal_year DESC ROWS UNBOUNDED PRECEDING) AS fiscal_week
        FROM
        (
            SELECT
                yr_idnt AS fiscal_year
                , MAX(wk_num) AS latest_wk
                , DENSE_RANK() OVER (ORDER BY yr_idnt DESC) AS yr_rank
            FROM bi_prd.style_group_daily_funnel fct

            JOIN bi_prd.dates dt
                ON fct.view_date = dt.day_dt

            WHERE dt.wk_end_day_dt <= (SELECT MAX(view_date) FROM bi_prd.style_group_daily_funnel fct)

            GROUP BY 1
          )
    ) ty_ly
    ON dt.fiscal_year = ty_ly.fiscal_year
    AND dt.wk_num_realigned <= ty_ly.fiscal_week
    AND ty_ly.ty_ly_ind != 'OTHER'

INNER JOIN bi_usr.bt0x_marketing_channel_hierarchy mch
  ON mch.join_channel = fct.mktg_channel

INNER JOIN bi_prd.product_hierarchy_current sdc
  ON fct.style_group_idnt = sdc.style_group_idnt

LEFT JOIN bi_usr.npp_brands_2019 npp
  ON sdc.brand = npp.brand

GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25
