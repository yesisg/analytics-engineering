 /*I used table name syntax as found in SQL portion of documentation and example from dbt/gitlab integration video. */

WITH sfdc_opportunity AS (
	
	SELECT * 
	FROM "ANALYTICS".analytics.sfdc_opportunity_xf
	
), month_avg_iacv
	
    SELECT
        , DATE_PART(month, close_date) AS close_month
        , AVG(incremental_acv) AS avg_iacv
    FROM sfdc_opportunity
    WHERE is_won = TRUE
        AND close_date >= DATE_TRUNC('year',CURRENT_DATE)
    GROUP BY 1
    ORDER BY 2 DESC
)

SELECT *
FROM month_avg_iacv
LIMIT 1