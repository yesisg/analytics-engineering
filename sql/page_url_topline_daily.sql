/*
----------------------------------------------------------------------------------------------------------------

Category Pages Topline at Daily Level
Returns topline metrics for all Category Pages (/c/), gift card, and stores by date, app_id, and page_urlpath)

>>> NOT RELATED TO MERCH CATEGORY OR BREADCRUMB <<<

Tables used:
    - clk_strm_sp.sp_page_views
    - clk_strm_sp.sp_link_clicks
    - clk_strm_sp.sp_orders

Metrics (by date, app_id, page_urlpath):
    - Page Views
    - Page Clicks
    - Page visitors
    - Page Sessions
    - Page Demand (directional)
    - Page Orders (directional)

----------------------------------------------------------------------------------------------------------------
*/

/*
---------------------------------------------------------------
Date Table
---------------------------------------------------------------
*/

DROP TABLE IF EXISTS page_url_tpl_dates;
--[[bi_prd.page_url_topline_daily]]
CREATE TEMPORARY TABLE page_url_tpl_dates AS (
  SELECT
      cast(trunc(convert_timezone('PST8PDT', GETDATE()))- 15 AS TIMESTAMP) AS etlstart --etl starttmstp
    , trunc(convert_timezone('PST8PDT', GETDATE())) - 15 AS derivedstart --derived_date start
    , trunc(convert_timezone('PST8PDT', GETDATE())) - 1 AS derivedend --derived_date end
);

/*
------------------------------
Get the cookie/day combos that define a bounce
------------------------------
*/
DROP TABLE IF EXISTS page_url_tpl_bouncers;

CREATE TEMPORARY TABLE page_url_tpl_bouncers
DISTSTYLE KEY
DISTKEY (domain_userid)
SORTKEY(derived_date)
AS
(
    SELECT
        domain_userid
        , derived_date
        , app_id
        , COUNT(domain_sessionid) AS page_views

    FROM clk_strm_sp.sp_page_views

    WHERE -- date filter
            etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
        AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
        AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
          -- other filters
        AND app_id IN ('n.com', 'nord.com', 'nord.mow')

    GROUP BY 1, 2, 3

    HAVING COUNT(domain_sessionid) = 1
);

/*
------------------------------
Get the URL path through which the cookie ENTERED
------------------------------
*/
DROP TABLE IF EXISTS page_url_tpl_entry;

CREATE TEMPORARY TABLE page_url_tpl_entry
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(derived_date)
AS
(
    SELECT
        entry_time.derived_date
        , entry_time.domain_userid
        , entry_time.app_id
        , entry_url.first_page_urlpath
        , COUNT(entry_time.*) AS entry_page_views

    FROM
        --Get the first timestamp for the cookie
        (
            SELECT
                domain_userid
                , derived_date
                , app_id
                , MIN(first_derived_tstamp) AS min_derived_tstamp

            FROM clk_strm_sp.sp_sessions

            WHERE -- date filter
                    etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                  -- other filters
                AND app_id IN ('n.com', 'nord.com', 'nord.mow')

            GROUP BY 1, 2, 3

        ) entry_time

        JOIN
        --Get the page url path and it's timestamp to join in and grab the first URL
        --that the cookie landed on
        (
            SELECT
                derived_date
                , domain_userid
                , app_id
                , LOWER(first_page_urlpath) AS first_page_urlpath
                , MIN(first_derived_tstamp) AS min_derived_tstamp

            FROM clk_strm_sp.sp_sessions

            WHERE -- date filter
                    etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                  -- other filters
                AND app_id IN ('n.com', 'nord.com', 'nord.mow')

            GROUP BY 1, 2, 3, 4

        ) entry_url
            ON entry_time.domain_userid = entry_url.domain_userid
            AND entry_time.min_derived_tstamp = entry_url.min_derived_tstamp
            AND entry_time.app_id = entry_url.app_id

    --We only care about people who landed on a content or category, gift-cards pages and homepage
    WHERE lower(split_part(entry_url.first_page_urlpath, '/', 2) ) IN (  'c', 'content', 'brands', 'nordstrom-gift-cards', 'gift-card', 'st', 'stores', 'store-detail')
        OR entry_url.first_page_urlpath = '/' --HOMEPAGE


    GROUP BY 1, 2, 3, 4
);

/*
------------------------------
Get the URL path through which the cookie EXITED
------------------------------
*/
DROP TABLE IF EXISTS page_url_tpl_exit;

CREATE TEMPORARY TABLE page_url_tpl_exit
DISTSTYLE KEY
DISTKEY(domain_userid)
SORTKEY(derived_date)
AS
(
    SELECT
        exit_time.derived_date
        , exit_time.domain_userid
        , exit_time.app_id
        , exit_url.last_page_urlpath
        , COUNT(exit_time.*) AS exit_page_views

    FROM
        --Get the last timestamp for the cookie
        (
            SELECT
                domain_userid
                , derived_date
                , app_id
                , MAX(first_derived_tstamp) AS max_derived_tstamp

            FROM clk_strm_sp.sp_sessions

            WHERE -- date filter
                    etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                  -- other filters
                AND app_id IN ('n.com', 'nord.com', 'nord.mow')

            GROUP BY 1, 2, 3

        ) exit_time

        JOIN
        --Get the page url path and it's timestamp to join in and grab the last URL
        --that the cookie landed on
        (
            SELECT
                derived_date
                , domain_userid
                , app_id
                , LOWER(first_page_urlpath) AS last_page_urlpath
                , MAX(first_derived_tstamp) AS max_derived_tstamp

            FROM clk_strm_sp.sp_sessions

            WHERE -- date filter
                    etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                  -- other filters
                AND app_id IN ('n.com', 'nord.com', 'nord.mow')

            GROUP BY 1, 2, 3, 4

        ) exit_url
            ON exit_time.domain_userid = exit_url.domain_userid
            AND exit_time.max_derived_tstamp = exit_url.max_derived_tstamp
            AND exit_time.app_id = exit_url.app_id

    --We only care about people who landed on a content or category, gift-cards pages and homepage
    WHERE lower(split_part(exit_url.last_page_urlpath, '/', 2) ) IN (  'c', 'content', 'brands', 'nordstrom-gift-cards', 'gift-card', 'st', 'stores', 'store-details')
        OR exit_url.last_page_urlpath = '/' --HOMEPAGE


    GROUP BY 1, 2, 3, 4
);

/*
------------------------------------------------------------------------------------------------------------------
    Main temp table for insert - page_url_tpl_insert

    Notes:
    [note1] - All engagement items like, Pagination, Sort, Top, Filters are removed from clk_strm_sp.sp_page_views
    [note2] - Only keeping category, content, rewards, gift-card pages and Homepage
    [note3] - domain_userid/sessiond_id join only on all pages that were visited before order was placed
    [note4] - Page Type: DLP, Category, Brands, Other
    [note5] - adding flag to identify (and remove) filtered category pages
            (learn more here > https://gitlab.nordstrom.com/nordace/internal-tools/snippets/wikis/page-type )
------------------------------------------------------------------------------------------------------------------
*/
DROP TABLE IF EXISTS  page_url_tpl_insert;
CREATE TEMPORARY TABLE page_url_tpl_insert
sortkey(activity_date)

AS
    SELECT
        pv.derived_date AS activity_date
        , dates.yr_idnt AS fiscal_year
        , dates.qtr_num AS fiscal_quarter
        , dates.half_num AS fiscal_half
        , dates.mth_num AS fiscal_month
        , dates.wk_num AS fiscal_week
        , dates.day_num AS fiscal_day
        , dates.day_idnt
        , dates.day_idnt_ly
        , dates.day_idnt_ly_realigned
        , dates.mth_desc AS fiscal_month_name
        , dates.wk_start_day_dt AS wk_start_date
        , dates.wk_end_day_dt AS wk_end_date
        , pv.app_id
        , CASE --ommiting gift-card serial number info
            WHEN pv.page_urlpath LIKE '/recipient-experience/challenge%'                THEN '/recipient-experience/challenge/'
            WHEN pv.page_urlpath LIKE '/recipient-experience/redemption%'               THEN '/recipient-experience/redemption/'
            WHEN pv.page_urlpath LIKE '/gift-card/view%'                                THEN '/gift-card/view/'
            WHEN pv.page_urlpath LIKE '/gift-card/edit%'                                THEN '/gift-card/edit/'
            WHEN pv.page_urlpath LIKE '/self_service/order/%'                           THEN '/self_service/order/'
            WHEN pv.page_urlpath LIKE '/self_service/login/%'                           THEN '/self_service/login/'
            WHEN pv.page_urlpath LIKE '/challenge/card/%'                               THEN '/challenge/card/'
            WHEN pv.filtered_categories = 1                                             THEN LEFT(pv.page_urlpath, REGEXP_INSTR(pv.page_urlpath, '/', 1, 3) -1)
            ELSE replace(--st/ to store-details/
                replace(--c/stores to stores/
                replace(pv.page_urlpath, '/content/', '/c/')
                                       , '/c/stores', '/stores')
                                       , '/st/', '/store-details/')
            END page_urlpath
        , max(clk.page_clicks) AS clicks
        , count(DISTINCT pv.domain_userid) AS visitors
        , count(DISTINCT pv.domain_sessionid) AS sessions
        , count(pv.domain_sessionid) AS page_views
        , sum(ord.demand) demand
        , sum(ord.orders) orders
        , sum(entry.entry_page_views) entry_page_views
        , sum(exit.exit_page_views) exit_page_views
        , count(bounce.domain_userid) one_page_visitors


    FROM
        ( -- page views
            SELECT
                  derived_date
                , domain_userid
                , domain_sessionid
                , app_id
                , lower(page_urlpath) page_urlpath
                , page_urlhost
                , derived_tstamp page_first_visit
                , CASE WHEN REGEXP_COUNT(page_urlpath, '/') = 3 AND LEFT(page_urlpath, 3) = '/c/'
                        THEN 1
                        ELSE 0
                        END AS filtered_categories --[note5]

            FROM clk_strm_sp.sp_page_views

            WHERE -- date filter
                    etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                  -- other filters
                AND app_id IN ('nord.com', 'n.com', 'nord.mow', 'nord.ios', 'nord.android')
                AND domain_userid <> ''
                AND domain_userid IS NOT NULL
                -- [note1] engagement items removed (MWP update: keep page=1)
                AND (bi_prd.f_get_urlquery_param(page_url, 'page') IS NULL OR bi_prd.f_get_urlquery_param(page_url, 'page') = 1)
                AND bi_prd.f_get_urlquery_param(page_url, 'sort')  IS NULL
                AND bi_prd.f_get_urlquery_param(page_url, 'top')   IS NULL
                AND bi_prd.f_get_urlquery_param(page_url, 'flexi') IS NULL
                -- [note2] only keeps category, content, gift-cards pages and homepage
                AND (
                    lower(split_part(page_urlpath, '/', 2)) IN (  'c', 'content', 'brands', 'nordstrom-gift-cards', 'gift-card', 'st', 'stores', 'store-details')
                    OR page_urlhost LIKE '%giftcard.nordstrom.com'
                    OR page_urlpath = '/' --HOMEPAGE
                    )

        ) pv

        LEFT JOIN
            ( --orders
                SELECT DISTINCT
                    derived_date
                    , domain_userid
                    , domain_sessionid
                    , app_id
                    , max(derived_tstamp) AS order_tstamp
                    , sum(tr_total) AS demand
                    , count(tr_orderid) AS orders

                FROM
                clk_strm_sp.sp_orders ord

                WHERE --date filter
                        etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                    AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                    AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                    -- other filters
                    AND app_id IN ('nord.com', 'n.com', 'nord.mow', 'nord.ios', 'nord.android')
                    AND domain_userid <> ''
                    AND domain_userid IS NOT NULL

                GROUP BY 1, 2, 3, 4
            )ord
                ON pv.derived_date = ord.derived_date
                AND pv.app_id = ord.app_id
                AND pv.domain_userid = ord.domain_userid
                AND pv.domain_sessionid = ord.domain_sessionid
                AND pv.page_first_visit <= ord.order_tstamp --[note3]

        LEFT JOIN
            ( --clicks
                SELECT
                      derived_date
                    , app_id
                    , lower(replace(page_urlpath, 'content/', 'c/')) AS page_urlpath
                    , count(domain_sessionid) page_clicks

                FROM clk_strm_sp.sp_link_clicks

                WHERE --date filter
                        etl_tstamp >= (SELECT etlstart FROM page_url_tpl_dates)
                    AND derived_date >= (SELECT derivedstart FROM page_url_tpl_dates)
                    AND derived_date <= (SELECT derivedend FROM page_url_tpl_dates)
                      -- other filters
                    AND app_id IN ('nord.com', 'n.com', 'nord.mow', 'nord.ios', 'nord.android')
                    AND domain_userid <> ''
                    AND domain_userid IS NOT NULL
                    -- [note2] only keeps category, content, gift-cards pages and homepage
                    AND (
                        lower(split_part(page_urlpath, '/', 2)) IN (  'c', 'content', 'brands', 'nordstrom-gift-cards', 'gift-card', 'st', 'stores', 'store-details')
                        OR page_urlhost LIKE '%giftcard.nordstrom.com'
                        OR page_urlpath = '/' --HOMEPAGE
                        )

                GROUP BY 1, 2, 3
            ) clk
                ON pv.derived_date = clk.derived_date
                AND pv.app_id = clk.app_id
                AND pv.page_urlpath = clk.page_urlpath

        LEFT JOIN page_url_tpl_entry entry
            ON pv.domain_userid = entry.domain_userid
            AND pv.derived_date = entry.derived_date
            AND pv.page_urlpath = replace(entry.first_page_urlpath, 'content/', 'c/')
            AND pv.app_id = entry.app_id

        LEFT JOIN page_url_tpl_exit exit
            ON pv.domain_userid = exit.domain_userid
            AND pv.derived_date = exit.derived_date
            AND pv.page_urlpath = replace(entry.first_page_urlpath, 'content/', 'c/')
            AND pv.app_id = exit.app_id

        LEFT JOIN page_url_tpl_bouncers bounce
            ON pv.domain_userid = bounce.domain_userid
            AND pv.derived_date = bounce.derived_date
            AND pv.app_id = bounce.app_id


        LEFT JOIN bi_prd.dates dates
            ON pv.derived_date = dates.day_dt

    -- added regex to trim all filtered categories back to normal page_urlpath
    --WHERE filtered_categories = 0 --excludes filtered categories

    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    ;

/*
--------------------------
  MERGE
--------------------------
*/
DELETE FROM bi_prd.page_url_topline_daily
WHERE
    activity_date >= (SELECT derivedstart FROM page_url_tpl_dates)
AND activity_date <=  (SELECT derivedend FROM page_url_tpl_dates)
;

/*-------------------------------------------------------------
 INSERT
 - outermost query is the only place where aggregation occurs.
 - Topliine metrics for all Category Pages
 - Page Type: DLP, Category, Brands, Other
----------------------------------------------------------------*/
INSERT INTO bi_prd.page_url_topline_daily
SELECT
   activity_date
  , fiscal_year
  , fiscal_quarter
  , fiscal_half
  , fiscal_month
  , fiscal_week
  , fiscal_day
  , day_idnt
  , day_idnt_ly
  , day_idnt_ly_realigned
  , fiscal_month_name
  , wk_start_date
  , wk_end_date
  , app_id
  , page_urlpath
  , clicks
  , visitors
  , sessions
  , page_views
  , demand
  , orders
  , entry_page_views
  , exit_page_views
  , one_page_visitors

 FROM page_url_tpl_insert;
