-- style group list for amp endpoint script ingestion
-- keeping only distincts from bi_prd.live_on_site

DROP TABLE IF EXISTS bi_prd.distinct_style_groups;
CREATE TABLE bi_prd.distinct_style_groups
diststyle even
sortkey(style_group_idnt)
AS
SELECT
DISTINCT style_grp_num AS style_group_idnt
, cast(trunc(convert_timezone('PST8PDT', getdate())) AS TIMESTAMP) AS etl_tstamp
, NULL AS was_scraped
FROM bi_prd.live_on_site
    
WHERE sku_live_date_end IS NULL
;

GRANT SELECT ON bi_prd.distinct_style_groups TO PUBLIC;
